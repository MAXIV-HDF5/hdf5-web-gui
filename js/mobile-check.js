/*global $*/
'use strict';

// Dumb check for a mobile device
window.mobilecheck = function () {

    var check = false;

    if ($(window).width() < 767) {
        check = true;
    }

    return check;
};
