/*global $*/
'use strict';


// External libraries
var AJAX_SPINNER, Plotly, HANDLE_DATASET, NAV_MENU, CONFIG_DATA, CAS_TICKET,

    // Global variables
    DATA_DISPLAY =
    {
        plotCanvasDiv : document.getElementById('plotCanvasDiv'),
        plotExists : false,
        colorScale : 'Hot',
        colorProfile : 'rgb(102,0,0)',
        plotLogValues : false,
        plotLogXValues : false,
        plotLogYValues : false,
        plotProjection : true,
        plotAutoLimits : true,
        plotHistogram: false,

        plotType : 'heatmap',
        plotDimension : 2,
        displayType : '',

        dataValues : [],
        dataProjectionValues: {xx: [], xy: [], yx: [], yy: []},
        initialDataValues : [],
        logOfDataValues : [],
        histogramValues : {},
        histogramLogValues : {},
        initialDataValuesBPR : [],
        logOfDataValuesBPR : [],
        badPixelsExist : false,

        stringDataValues : [],
        resizeTimer : undefined,
        plotWidth : 550,
        plotHeight : 550,
        useDarkTheme : true,
        mobileDisplay : false,

        imageSeries : false,
        imageSeriesRange : 0,
        imageSeriesIndex : false,
        imageTargetUrl : undefined,
        imageShapeDims : undefined,
        imageIsDownsampled : false,
        imageZoomSection : false,
        usingOriginalImage : true,
        imageTitle : 'Title goes here',
        dataPath : '',

        lineYValues : [],
        lineXValues : undefined,
        lineTitle : '',

        barYValues : [],
        barXValues : [],
        barXBins : {},

        loadedImageRange : undefined,
        loadedImageRangeSize : [0, 0],
        loadedImageSize : undefined,

        plotAspect : false,

        // Clear the plotting canvas along with whatever objects were there
        purgePlotCanvas : function () {
            Plotly.purge(DATA_DISPLAY.plotCanvasDiv);
        },


        showPlotCanvas : function () {
            $('#plotCanvasDiv').removeClass('hidden');
            $('#welcomeDiv').addClass('hidden');
        },


        // Enable or disable various image and image series controls
        enableImagePlotControls : function (enablePlotControls,
            enableImageControls, enableSeriesControls,
            enableHistogramControls) {

            var i, debug = false, seriesMax = 0, endButtonWidth = '50px',
                plotControlDiv = [ '#plotControlDownload',
                    '#plotControlReset', '#plotControlShare'],
                // plotControlDiv = [ '#plotControlDownload'],
                imageControlDiv = ['#plotControlColor', '#plotControlOptions',
                    '#plotControlAspect', '#plotControlAutoImage',
                    '#plotOptionLog'],
                imageControlDivMobile = ['#plot-control-mobile'],
                seriesControlDiv = ['#imageSeriesControl'];

            if (debug) {
                console.debug('*** START js/data-display.js' +
                    ' enableImagePlotControls');
                console.log('  enableImageControls:  ' + enableImageControls);
                console.log('  enableSeriesControls: ' + enableSeriesControls);
            }

            // Show or hide the automatic intensity limits button -
            // plotControlAutoImage
            if (!DATA_DISPLAY.badPixelsExist) {
                imageControlDiv = ['#plotOption3D', '#plotControlAspect',
                    '#plotControlColor', '#plotControlOptions'];
            }

            // Show or hide square pixel option
            if (DATA_DISPLAY.plotDimension === 3) {
                imageControlDiv = ['#plotOption3D', '#plotControlColor',
                    '#plotControlOptions', '#plotControlAutoImage'];
            }

            if (enableHistogramControls) {
                // Keep the plot option dropwdown if we're plotting histogram
                imageControlDiv = ['#plotControlColor', '#plotControlAspect',
                    '#plotControlAutoImage', '#plotOptionLog'];
            } else if (DATA_DISPLAY.plotHistogram) {
                // Remove check marks, unusable plot options and reset vars
                DATA_DISPLAY.hideHistogram(false);
            }

            if (DATA_DISPLAY.mobileDisplay) {
                // Mobile image controls - show, hide
                for (i = 0; i < imageControlDivMobile.length; i += 1) {
                    $(imageControlDivMobile[i]).toggleClass('hidden',
                        !enableImageControls);
                }
            } else {
                // General image controls - show, hide
                for (i = 0; i < imageControlDiv.length; i += 1) {
                    $(imageControlDiv[i]).toggleClass('hidden',
                        !enableImageControls);
                }

                // General plot controls - show, hide
                for (i = 0; i < plotControlDiv.length; i += 1) {
                    $(plotControlDiv[i]).toggleClass('hidden',
                        !enablePlotControls);
                }
            }

            // Image series controls - show, hide
            for (i = 0; i < seriesControlDiv.length; i += 1) {
                $(seriesControlDiv[i]).toggleClass('hidden',
                    !enableSeriesControls);
            }

            // We don't want to change the display info in the histogram
            // plot, since we don't change the actual image
            if (!DATA_DISPLAY.plotHistogram) {
                DATA_DISPLAY.imageSeries = enableSeriesControls;
            }

            if (enableSeriesControls) {

                seriesMax = DATA_DISPLAY.imageSeriesRange - 1;

                // Reset the value and limits of the input field
                $("#inputNumberDiv").val(DATA_DISPLAY.imageSeriesIndex);
                $('#inputNumberDiv').attr({
                    'min' : 0,
                    'max' : seriesMax,
                });

                $("#imageSeriesSlider").val(DATA_DISPLAY.imageSeriesIndex);
                $('#imageSeriesSlider').attr({
                    'min' : 0,
                    'max' : seriesMax,
                });

                // Set the text of the start and end buttons
                $('#startButtonValue').text(0);
                $('#endButtonValue').text(' ' + seriesMax);

                // Set the width of the end button, depending on text size
                endButtonWidth = seriesMax.toString().length * 10 + 40;
                endButtonWidth = parseInt(endButtonWidth, 10) + 'px';

                if (debug) {
                    console.log('endButtonWidth: ' + endButtonWidth);
                }

                $('#endButton').css("width", endButtonWidth);
            }
        },


        // Display text
        cleanupStringForDisplay : function (inputString) {

            var debug = false, outputArray, outputString, len, curr, prev,
                element;


            if (debug) {
                console.log('inputString: ' + inputString);
            }

            // Convert to strings, remove bad, bad things
            inputString = String(inputString);
            inputString = inputString.replace(/\$/g, '');

            // Check for empty values
            if (inputString === '') {
                inputString = '<empty value>';
            }

            // Remove leading, trailing, and multiple spaces
            inputString = inputString.replace(/ +(?= )/g, '');
            inputString = inputString.trim();

            // For long strings, split em up
            if (inputString.length > 49) {
                len = 39;
                curr = len;
                prev = 0;

                outputArray = [];

                while (inputString[curr]) {
                    curr += 1;
                    if (inputString[curr] === ' ') {

                        element = inputString.substring(prev, curr);

                        // Remove leading, trailing, and multiple spaces
                        element = element.replace(/ +(?= )/g, '');
                        element = element.trim();

                        outputArray.push(element);

                        prev = curr;
                        curr += len;
                    }
                }

                element = inputString.substring(prev);

                // Remove leading, trailing, and multiple spaces
                element = element.replace(/ +(?= )/g, '');
                element = element.trim();

                outputArray.push(element);

                if (debug) {
                    console.log(outputArray);
                }

                // Create one long string from all the pieces, inserting
                // HTML line breaks between each
                outputString = outputArray.join('<br>');

            } else {
                outputString = inputString;
            }

            if (debug) {
                console.log('outputString: ' + outputString);
            }

            return outputString;
        },


        // Display text
        drawText : function (itemTitle, itemValue, fontColor) {

            var debug = false, data, layout, options, mainDataPlot, string1,
                string2;

            if (debug) {
                console.debug('*** START js/data-display.js drawText ***');
                console.log('  itemTitle: ' + itemTitle);
                console.log('  itemValue: ' + itemValue);
                console.log('  fontColor: ' + fontColor);
            }

            DATA_DISPLAY.calculatePlotSize();
            DATA_DISPLAY.showPlotCanvas();

            DATA_DISPLAY.enableImagePlotControls(false, false, false);

            DATA_DISPLAY.displayType = 'text';

            // Clean up the strings, and if they are long strings, they ought
            // to be split up
            string1 = DATA_DISPLAY.cleanupStringForDisplay(itemTitle);
            string2 = DATA_DISPLAY.cleanupStringForDisplay(itemValue);

            if (debug) {
                console.log('itemTitle  -->  itemValue:');
                console.log(itemTitle + ' --> ' + itemValue);
                console.log('string1  -->  string2:');
                console.log(string1 + ' --> ' + string2);
            }

            // Check for color choice
            fontColor = (fontColor === false ? '#ad3a3a' : fontColor);

            // Setup the empty data
            mainDataPlot = {
                z: [],
                colorscale: DATA_DISPLAY.colorScale,
            };

            // All the data that is to be plotted
            data = [mainDataPlot];

            // The layout of the plotting canvas and axes.
            layout = {
                "title" : '',
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "showlegend" : false,
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : 300,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),

                xaxis: {
                    title: '',
                    showgrid: false,
                    zeroline: false,
                    showticklabels : false,
                    ticks : '',
                },

                yaxis: {
                    title: '',
                    showgrid: false,
                    zeroline: false,
                    showticklabels : false,
                    ticks : '',
                },

                // More annotation examples here:
                //  https://plot.ly/javascript/text-and-annotations/
                annotations: [
                    {
                        x: 0,
                        y: 0,
                        xref: 'x',
                        yref: 'y',
                        text: '<b>' + string1 + '</b>' + '<br>' + string2,
                        showarrow: false,
                        font: {
                            family: 'Courier New, monospace',
                            size: 16,
                            color: fontColor,
                        },
                        align: 'center',
                        bordercolor: fontColor,
                        borderwidth: 3,
                        borderpad: 4,
                        bgcolor: 'rgba(255, 255, 255, 0.9)',
                        opacity: 0.8
                    }
                ],
            };

            options = {
                staticPlot: true,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                displayModeBar: false,
                showTips: false,
            };

            DATA_DISPLAY.purgePlotCanvas();

            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout, options
                ).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

            if (debug) {
                console.debug('***  END  js/data-display.js drawText ***');
            }
        },


        displayBadDataMessage : function () {
            DATA_DISPLAY.enableImagePlotControls(false, false, false);
            DATA_DISPLAY.drawText('Something seems to wrong with this dataset',
                'Sorry for the inconvenience :(',
                '#6b0909', '');
        },

        displayErrorMessage : function (message) {
            DATA_DISPLAY.enableImagePlotControls(false, false, false);

            if (message === "" || message === undefined) {
                DATA_DISPLAY.drawText('I don\'t know how to handle this yet!',
                    'Sorry for the inconvenience :(',
                    '#ad3a74', '');
            } else {
                DATA_DISPLAY.drawText(message,
                    'Sorry for the inconvenience :(',
                    '#ad3a74', '');
            }
        },

        generateImageTitle : function (zMin, zMax) {
            var imageTitle;

            imageTitle = "<b>" + DATA_DISPLAY.imageTitle + "</b>";

            // Add the image index if it's an image series
            if (DATA_DISPLAY.imageSeries) {
                imageTitle += '-' +
                    DATA_DISPLAY.imageSeriesIndex;
            }

            // Remove unnecessary percision
            zMin = zMin.toPrecision(4);
            zMax = zMax.toPrecision(4);

            // Add resolution to the image title
            if (DATA_DISPLAY.plotDimension === 2) {
                imageTitle += "<br><b>Full Image Size:</b> "
                              + DATA_DISPLAY.imageShapeDims[0]
                              + "x" + DATA_DISPLAY.imageShapeDims[1];
            }

            // Add max, min and size values to the image title
            if (DATA_DISPLAY.plotLogValues) {
                imageTitle += "<br><b>Instensity Range</b> = [" +
                              "10<sup>" + zMin + "</sup>; " +
                              "10<sup>" + zMax + "</sup>]";
            } else {
                imageTitle += "<br><b>Instensity Range</b>" +
                              " = [" + zMin + "; " + zMax + "]";
            }

            return imageTitle;
        },

        drawLine : function (xValues, yValues, layout) {

            var debug = false, data, fullLayout, options;

            // Create data object
            data = [
                {
                    "x": xValues,
                    "y" : yValues,
                    "mode" : 'lines',
                    "type" : 'scatter'
                }
            ];

            // And the layout
            fullLayout = {
                "showlegend" : false,
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : layout.title),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : DATA_DISPLAY.plotHeight,
                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "margin" : {
                    l: 65,
                    r: 50,
                    b: 65,
                    t: 90,
                },
                "xaxis" : {
                    "title" : layout.xLabel,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
                "yaxis" : {
                    "title" : layout.yLabel,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                }
            };

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                // displayModeBar: true,
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            // Present them
            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, fullLayout,
                    options).then(AJAX_SPINNER.doneLoadingData());
            DATA_DISPLAY.plotExists = true;

            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout line');
                    }

                    DATA_DISPLAY.handle1DZoom(eventdata);
                });
        },


        draw3DPlot : function () {

            var debug = false, data, layout, options, plotMargins, profiles,
                imageTitle;

            // Get the proper x & y axes ranges if this image has been
            // downsampled
            // profiles = DATA_DISPLAY.fillProfileProjections(
            //     [0, DATA_DISPLAY.imageShapeDims[0],
            //         0, DATA_DISPLAY.imageShapeDims[1]]
            // );

            profiles = DATA_DISPLAY.fillProfileProjections(
                DATA_DISPLAY.imageZoomSection
            );

            if (debug) {
                console.log('profiles.zMax: ' + profiles.zMax);
                console.log('profiles.zMin: ' + profiles.zMin);
            }

            // Create data object
            data = [
                {
                    z: DATA_DISPLAY.dataValues,
                    type: DATA_DISPLAY.plotType,
                    zsmooth: true,  // this might make plot interaction faster
                                    // if set true - not sure though
                    zauto : true,
                    zmin: profiles.zMin,
                    zmax: profiles.zMax,
                    cmin: profiles.zMin,
                    cmax: profiles.zMax,
                    x: profiles.imageXAxis,
                    y: profiles.imageYAxis,

                    // For mouse-over hover information, show the data values
                    // in the text field so that the same values appear when
                    // displaying the log values
                    hoverinfo: 'x+y+text',
                    text: DATA_DISPLAY.stringDataValues,

                    colorscale: DATA_DISPLAY.colorScale,
                    showscale : !DATA_DISPLAY.mobileDisplay,
                    colorbar : {
                        "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                        "titleside" : "bottom",
                        "exponentformat" : "power",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                }
            ];

            plotMargins =  { l: 65, r: 50, b: 65, t: 90, };
            if (DATA_DISPLAY.mobileDisplay) {
                plotMargins =  { l: 30, r: 20, b: 30, t: 20, };
            }

            // Update the image title with max, min and resolution
            imageTitle = DATA_DISPLAY.generateImageTitle(profiles.zMin,
                profiles.zMax);

            // And the layout
            layout = {
                "showlegend" : false,
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : imageTitle),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#999" : "#000000"),
                    "size" : 14
                },
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : DATA_DISPLAY.plotHeight,
                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "margin" : plotMargins,
                "scene" : {
                    "xaxis" : {
                        "title" : "x",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                    "yaxis" : {
                        "title" : "y",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    },
                    "zaxis" : {
                        "title" : "z",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                        "type" : "linear",
                        "autorange" : true
                    }
                }
            };

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                // displayModeBar: true,
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            // Present them
            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout,
                options).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

            // Refill the profile projections when a zoom event occurs
            // Why isn't this properly done already in the plotly library?!
            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout ' + DATA_DISPLAY.plotType);
                    }

                    DATA_DISPLAY.handle3DZoom(eventdata);
                });
        },


        // Fill x and y profile projections, given the image and the dimensions
        // of the section of the image being viewed
        fillProfileProjections : function (ranges) {

            var debug = false, i, j,
                imageYAxis = [], imageXAxis = [],
                xFactor = 1, yFactor = 1,
                zMin = 1e100, zMax = -1e100, xMin = ranges[0],
                xMax = ranges[1], yMin = ranges[2], yMax = ranges[3],
                xCoordinate, yCoordinate, dataValue;

            DATA_DISPLAY.dataProjectionValues = {xx: [], xy: [], yx: [],
                yy: []};

            // Check if this is a downsampled image - if so, change the axes
            // ranges
            if (DATA_DISPLAY.imageIsDownsampled) {
                xFactor = (xMax - xMin) / DATA_DISPLAY.dataValues[0].length;
                yFactor = (yMax - yMin) / DATA_DISPLAY.dataValues.length;
            }

            if (debug) {
                console.log('xFactor: ' + xFactor);
                console.log('yFactor: ' + yFactor);
                console.log('xMin:           ' + xMin);
                console.log('xMax:           ' + xMax);
                console.log('yMin:           ' + yMin);
                console.log('yMax:           ' + yMax);
                console.log('xFactor:        ' + xFactor);
                console.log('yFactor:        ' + yFactor);
                console.log('DATA_DISPLAY.dataValues.length: ' +
                    DATA_DISPLAY.dataValues.length);
                console.log('DATA_DISPLAY.dataValues[i].length: ' +
                    DATA_DISPLAY.dataValues[0].length);
                console.log('DATA_DISPLAY.loadedImageRange: ' +
                    DATA_DISPLAY.loadedImageRange);
                console.log('DATA_DISPLAY.loadedImageRangeSize: ' +
                    DATA_DISPLAY.loadedImageRangeSize);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
            }

            // Fill profile projections
            for (i = 0; i < DATA_DISPLAY.dataValues.length; i += 1) {

                yCoordinate = DATA_DISPLAY.loadedImageRange[2] +
                    yFactor * i;

                // The y-profile values
                imageYAxis[i] = Math.round(yCoordinate);

                if (DATA_DISPLAY.plotProjection) {
                    DATA_DISPLAY.dataProjectionValues.yx[i] = yCoordinate;
                    DATA_DISPLAY.dataProjectionValues.yy[i] = 0;
                }

                for (j = 0; j < DATA_DISPLAY.dataValues[i].length; j += 1) {

                    xCoordinate = DATA_DISPLAY.loadedImageRange[0]
                        + j * xFactor;

                    // The x-profile values
                    if (i === 0) {
                        imageXAxis[j] = Math.round(xCoordinate);

                        if (DATA_DISPLAY.plotProjection) {
                            DATA_DISPLAY.dataProjectionValues.xx[j] =
                                xCoordinate;
                            DATA_DISPLAY.dataProjectionValues.xy[j] = 0;
                        }
                    }

                    // Fill projections for displayed image only
                    if (yCoordinate >= yMin && yCoordinate < yMax &&
                            xCoordinate >= xMin && xCoordinate < xMax) {

                        dataValue = DATA_DISPLAY.dataValues[i][j];

                        // Find the max and min values - used for setting
                        // colorscale range
                        if (dataValue < zMin && dataValue !== null) {
                            zMin = dataValue;
                        }
                        if (dataValue > zMax && dataValue !== null) {
                            zMax = dataValue;
                        }

                        if (DATA_DISPLAY.plotProjection) {
                            // Fill the profile historgrams
                            DATA_DISPLAY.dataProjectionValues.xy[j] +=
                                dataValue;
                            DATA_DISPLAY.dataProjectionValues.yy[i] +=
                                dataValue;
                        }

                    }
                }
            }

            if (debug) {
                console.log('zMin: ' + zMin);
                console.log('zMax: ' + zMax);
                console.log('DATA_DISPLAY.plotLogValues: ' +
                    DATA_DISPLAY.plotLogValues);
            }

            return {
                imageYAxis : imageYAxis,
                imageXAxis : imageXAxis,
                zMin : zMin,
                zMax : zMax,
            };

        },


        // Check if an event is a zoom event
        isZoomEvent : function (eventdata) {

            var debug = false, i = 0, zoomEvent = false,
                rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                    'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                console.log(JSON.stringify(eventdata));
            }

            // Zoom events return json objects containing keys like
            // 'xaxis.range' or 'xaxis.autorange'
            for (i = 0; i < rangeKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(rangeKeys[i])) {
                    zoomEvent = true;
                }
            }

            for (i = 0; i < autoKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(autoKeys[i])) {
                    zoomEvent = true;
                }
            }

            if (debug) {
                console.log('** DATA_DISPLAY.isZoomEvent Does not Look' +
                    ' like a zoom event');
            }

            return zoomEvent;
        },


        getZoomRange : function (eventdata) {

            var debug = false, i = 0, ranges = [-1, -1, -1, -1],
                plotLayout, rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                // Get the present layout range
                plotLayout = DATA_DISPLAY.plotCanvasDiv.layout;
                console.log('plotLayout: ');
                console.log(plotLayout);
            }

            // Loop over the 4 range values - x & y, min & max
            for (i = 0; i < ranges.length; i += 1) {

                // Look at the 'range' keys, set ranges
                if (eventdata.hasOwnProperty(rangeKeys[i])) {

                    ranges[i] = eventdata[rangeKeys[i]];

                    if (debug) {
                        console.log(rangeKeys[i]);
                        console.log(eventdata[rangeKeys[i]]);
                    }

                }

                // If any new min or max has not yet been set, then
                // use the present values
                if (ranges[i] === -1) {
                    ranges[i] = DATA_DISPLAY.loadedImageRange[i];
                }

                // Round to the nearest integer
                ranges[i] = Math.round(ranges[i]);

                // Need to add 1 to end of ranges
                if (!DATA_DISPLAY.isEven(i)) {
                    ranges[i] += 1;
                    if (debug) {
                        console.log('ranges[' + i + ']: ' + ranges[i]);
                    }
                }
            }

            // Make sure we haven't gone too far
            if (ranges[0] < 0) {
                ranges[0] = 0;
            }
            if (ranges[1] >= DATA_DISPLAY.imageShapeDims[0]) {
                ranges[1] = DATA_DISPLAY.imageShapeDims[0];
            }
            if (ranges[2] < 0) {
                ranges[2] = 0;
            }
            if (ranges[3] >= DATA_DISPLAY.imageShapeDims[1]) {
                ranges[3] = DATA_DISPLAY.imageShapeDims[1];
            }


            // Check for reset-zoom events
            if (eventdata.hasOwnProperty(autoKeys[0])) {
                ranges[0] = 0;
                ranges[1] = DATA_DISPLAY.imageShapeDims[0];
            }
            if (eventdata.hasOwnProperty(autoKeys[1])) {
                ranges[2] = 0;
                ranges[3] = DATA_DISPLAY.imageShapeDims[1];
            }

            if (debug) {
                console.log('x-axis start: ' + ranges[0]);
                console.log('x-axis end:   ' + ranges[1]);
                console.log('y-axis start: ' + ranges[2]);
                console.log('y-axis end:   ' + ranges[3]);
            }

            // Save for later
            DATA_DISPLAY.imageZoomSection = ranges;

            return ranges;
        },


        // Do what it takes to handle a zoom event in a 3D image
        handle3DZoom : function (eventdata) {

            var debug = false, promises = [], newImageFetched = false,
                resetZoomEvent = false;

            if (debug) {
                console.log(JSON.stringify(eventdata));
            }

            // Try and guess if the 'Rest camera to default' button has been
            // pressed - there must be a better way!
            // If so, get a brand spanking new image, which is necessary if
            // we are already zoomed in on a previously downsampled image.
            if (eventdata.hasOwnProperty('scene')) {
                if (eventdata.scene.hasOwnProperty('eye')) {
                    if (eventdata.scene.eye.x === 1.25 &&
                            eventdata.scene.eye.y === 1.25 &&
                            eventdata.scene.eye.z === 1.25) {

                        if (debug) {
                            console.log('reset button pressed?');
                        }
                        resetZoomEvent = true;

                    }
                }
            }

            if (resetZoomEvent) {

                // For an image series
                if (DATA_DISPLAY.imageSeries) {
                    promises.push(
                        HANDLE_DATASET.imageSeriesInput(
                            DATA_DISPLAY.imageSeriesIndex,
                            false,
                            false,
                            true
                        )
                    );

                // For an image
                } else {
                    promises.push(
                        HANDLE_DATASET.requestForData(DATA_DISPLAY.dataPath,
                            false, false, false)
                    );
                }

                newImageFetched = true;

                if (debug) {
                    console.log('newImageFetched: ' +
                        newImageFetched);
                }

                if (newImageFetched) {

                    $.when.apply(null, promises).done(
                        function () {
                            DATA_DISPLAY.updatePlotZData(false,
                                newImageFetched,
                                true);
                        }
                    );
                }

            }

        },

        // Do what it takes to handle a zoom event in a 2D image
        handle1DZoom : function (eventdata) {

            var debug = false;

            // Check if this is a zoom event
            if (!DATA_DISPLAY.isZoomEvent(eventdata)) {
                if (debug) {
                    console.log('** DATA_DISPLAY.handle1DZoom Does not Look' +
                        ' like a zoom event, exiting');
                }
                return;
            }

            if (debug) {
                console.log('** DATA_DISPLAY.handle1DZoom Plot zoom event **');
                console.log(eventdata);
            }

        },

        // Handle a 2D image.  A new image is always fetched, even if the
        // presently displayed image has not been decimated, as it makes
        // several other operations simpler.
        handle2DZoom : function (eventdata) {

            var debug = false, i = 0, ranges = [-1, -1, -1, -1], section,
                resetZoomEvent = false,
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                console.debug('*** START DATA_DISPLAY.handle2DZoom ***');
            }

            // Check if this is a zoom event
            if (!DATA_DISPLAY.isZoomEvent(eventdata)) {
                return;
            }

            // Get the zoom range
            ranges = DATA_DISPLAY.getZoomRange(eventdata);

            // Check for reset-zoom events
            for (i = 0; i < autoKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(autoKeys[i])) {
                    resetZoomEvent = true;
                }
            }

            // Show the 'Reset Zoom' button
            if (resetZoomEvent === false) {
                $('#plotControlReset').show();
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageSeries:        ' +
                    DATA_DISPLAY.imageSeries);
                console.log('DATA_DISPLAY.imageIsDownsampled: ' +
                    DATA_DISPLAY.imageIsDownsampled);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
                console.log('                 resetZoomEvent: ' +
                    resetZoomEvent);
            }

            // Start the loading thingy
            AJAX_SPINNER.startLoadingData(10);

            // For an image series, alter the slice notation
            if (DATA_DISPLAY.imageSeries) {
                section = [DATA_DISPLAY.imageSeriesIndex,
                          DATA_DISPLAY.imageSeriesIndex + 1,
                          ranges[0], ranges[1], ranges[2], ranges[3]];
            } else {
                section = ranges;
            }

            // Save the zoom/section information
            DATA_DISPLAY.loadedImageRange = ranges;

            // Get the requested data and plot it
            HANDLE_DATASET.requestForData(DATA_DISPLAY.dataPath, section,
                false, false);

            if (debug) {
                console.debug('***  END  DATA_DISPLAY.handle2DZoom ***');
            }
        },


        // Plot the image as a 2D heatmap along with x and y profile projection
        // that update when zooming
        draw2DPlot : function () {

            var debug = false, profiles, xProfilePlot, yProfilePlot, data,
                layout, options, mainDataPlot, mainAxisPercent,
                plotMargins = {}, imageTitle;

            if (debug) {
                console.debug('** START DATA_DISPLAY.draw2DPlot  ***');
                console.log('DATA_DISPLAY.imageShapeDims[0]' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]' +
                    DATA_DISPLAY.imageShapeDims[1]);
            }

            // Create profile projections and calculate the range of the axes
            profiles = DATA_DISPLAY.fillProfileProjections(
                DATA_DISPLAY.imageZoomSection
            );

            // The primary, 2-dimensional plot of the data - works best as a
            // 'heatmap' plot me thinks
            mainDataPlot = {
                z: DATA_DISPLAY.dataValues,
                type: DATA_DISPLAY.plotType,
                zsmooth: false,
                zmin: profiles.zMin,
                zmax: profiles.zMax,
                x: profiles.imageXAxis,
                y: profiles.imageYAxis,

                // For mouse-over hover information, show the data values in
                // the text field so that the same values appear when
                // displaying the log values
                hoverinfo: 'x+y+text',
                text: DATA_DISPLAY.stringDataValues,

                colorscale: DATA_DISPLAY.colorScale,
                showscale : !DATA_DISPLAY.mobileDisplay,
                colorbar : {
                    "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                    "titleside" : "bottom",
                    "exponentformat" : "power",
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
            };

            if (debug) {
                console.log('DATA_DISPLAY.colorProfile: ' +
                    DATA_DISPLAY.colorProfile);
            }

            // All the data that is to be plotted
            data = [mainDataPlot];

            // Padding around the plotting canvas - less for mobile devices
            plotMargins =  { l: 65, r: 50, b: 65, t: 90, };
            if (DATA_DISPLAY.mobileDisplay) {
                plotMargins =  { l: 30, r: 20, b: 30, t: 20, };
            }

            // Update the image title with max, min and resolution
            imageTitle = DATA_DISPLAY.generateImageTitle(profiles.zMin,
                profiles.zMax);

            // Main plot's domain top value i.e. precent it covers
            // (lower if we have projections)
            mainAxisPercent = DATA_DISPLAY.plotProjection === true ? 0.85 : 1;

            // The layout of the plotting canvas and axes. Note that the amount
            // of space each plot takes up is a range from 0 to 1, and follows
            // the keyword 'domain'
            layout = {
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : imageTitle),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                    "size" : 14
                },
                "showlegend" : false,
                "autosize" : false,

                "width": DATA_DISPLAY.plotWidth,
                "height": DATA_DISPLAY.plotHeight,
                "margin": plotMargins,

                "hovermode" : 'closest',
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),

                "xaxis" : {
                    "title" : "x",
                    "domain" : [0, mainAxisPercent],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },

                    // Settings for square pixels
                    "constraintoward" : "center",
                },

                "yaxis" : {
                    "title" : "y",
                    "domain" : [0, mainAxisPercent],
                    "showgrid" : false,
                    "zeroline": false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },

                    // Settings for square pixels
                    "scaleanchor" : (DATA_DISPLAY.plotAspect === true ?
                                "x" : ""),
                    "scaleratio" : 1,
                    "constraintoward" : "middle",
                },
            };

            if (DATA_DISPLAY.plotProjection) {
                // Refactor as a function addProjectionData??

                // The x-profile of the plot, displayed as a bar chart
                xProfilePlot = {
                    x: DATA_DISPLAY.dataProjectionValues.xx,
                    y: DATA_DISPLAY.dataProjectionValues.xy,
                    xmax: 500,
                    ymax: 500,
                    zmax: 500,
                    name: 'x profile',
                    marker: {color: DATA_DISPLAY.colorProfile},
                    yaxis: 'y2',
                    type: 'bar'
                };

                // The y-profile, also displayed as a bar chart,
                // oriented horizontally --> switch x & y
                yProfilePlot = {
                    x: DATA_DISPLAY.dataProjectionValues.yy,
                    y: DATA_DISPLAY.dataProjectionValues.yx,
                    name: 'y profile',
                    marker: {color: DATA_DISPLAY.colorProfile},
                    xaxis: 'x2',
                    type: 'bar',
                    orientation: 'h'
                };

                data.push(xProfilePlot, yProfilePlot);

                layout.xaxis2 = {
                    "domain" : [mainAxisPercent, 1],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                };

                layout.yaxis2 = {
                    "domain" : [mainAxisPercent, 1],
                    "showgrid" : false,
                    "zeroline" : false,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                };
            }

            if (debug) {
                console.log('layout:');
                console.log(layout);
            }

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };
            // All options are here:
            //  https://github.com/plotly/plotly.js/blob/master/src/plot_api/
            //      plot_config.js
            //  https://github.com/plotly/plotly.js/blob/master/src/components/
            //      modebar/buttons.js

            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, layout,
                options).then(
                AJAX_SPINNER.doneLoadingData()
            );
            DATA_DISPLAY.plotExists = true;

            // Refill the profile projections when a zoom event occurs
            // Why isn't this properly done already in the plotly library?!
            DATA_DISPLAY.plotCanvasDiv.on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvasDiv' +
                            'plotly_relayout ' + DATA_DISPLAY.plotType);
                    }

                    DATA_DISPLAY.handle2DZoom(eventdata);
                });

            if (debug) {
                console.debug('**  END  DATA_DISPLAY.draw2DPlot  ***');
            }
        },


        redrawPlotCanvas : function (timeDelay) {

            var debug = false, plotHeight = DATA_DISPLAY.plotHeight;

            if (DATA_DISPLAY.plotExists) {

                // During a window resize event, the resize function will be
                // called several times per second, on the order of 15 Hz! Best
                // to wait a bit try to just resize once, as it's a bit costly
                // for plotly to execute relayout
                clearTimeout(DATA_DISPLAY.resizeTimer);

                AJAX_SPINNER.startLoadingData(50);

                DATA_DISPLAY.resizeTimer = setTimeout(function () {

                    if (debug) {
                        console.log('about to run Plotly.relayout');
                    }

                    // Calculate the plot dimensions and save them
                    DATA_DISPLAY.calculatePlotSize();

                    // Use smaller canvas when displaying text instead of
                    // images
                    if (DATA_DISPLAY.displayType === 'text') {
                        plotHeight = 300;
                    } else {
                        plotHeight = DATA_DISPLAY.plotHeight;
                    }

                    Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                        width: DATA_DISPLAY.plotWidth,
                        height: plotHeight,
                    }).then(
                        AJAX_SPINNER.doneLoadingData()
                    );

                }, timeDelay);
            }

        },


        // Calculate the plot size - needs to be improved for small screens
        calculatePlotSize : function () {

            var debug = false, newPlotDivHeight, newPlotDivWidth,
                windowWidth = $(window).width(),
                windowHeight = $(window).height(),
                containerWidth = $('#displayContainer').width(),
                containerHeight = $('#displayContainer').height(),
                divWidth = $('#plotCanvasDiv').width(),
                divHeight = $('#plotCanvasDiv').height(),
                sideNavMenuWidth = $('#side-nav-menu').width();

            if (debug) {
                console.debug('*** START data-display.js calculatePlotSize');
            }

            newPlotDivHeight = windowHeight - 80;
            if (DATA_DISPLAY.imageSeries) {
                newPlotDivHeight -= 33;
            }

            //////////////////////////////////////////////////////////////
            NAV_MENU.displayOffsetWidth =
                (document.getElementById("side-nav-menu").offsetWidth + 5) +
                "px";

            if (debug) {
                console.log("NAV_MENU.displayOffsetWidth: " +
                    NAV_MENU.displayOffsetWidth);
            }

            // Slide page content out so that it is not hidden by the
            // menu
            document.getElementById(NAV_MENU.displayContainer
                ).style.marginLeft = NAV_MENU.displayOffsetWidth;


            $('#displayContainer').width(windowWidth -
                (document.getElementById("side-nav-menu").offsetWidth + 15));

            containerWidth = $('#displayContainer').width();
            //////////////////////////////////////////////////////////////

            newPlotDivWidth = containerWidth;

            // For smaller screens, no padding
            DATA_DISPLAY.mobileDisplay = window.mobilecheck();
            if (DATA_DISPLAY.mobileDisplay) {
                newPlotDivWidth -= 40;
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageSeries: ' +
                    DATA_DISPLAY.imageSeries);
                console.log('DATA_DISPLAY.mobileDisplay: ' +
                    DATA_DISPLAY.mobileDisplay);
                console.log('windowWidth:      ' + windowWidth);
                console.log('windowHeight:     ' + windowHeight);
                console.log('divWidth:         ' + divWidth);
                console.log('divHeight:        ' + divHeight);
                console.log('containerWidth:   ' + containerWidth);
                console.log('sideNavMenuWidth: ' + sideNavMenuWidth);
                console.log('containerHeight:  ' + containerHeight);
                console.log('newPlotDivHeight: ' + newPlotDivHeight);
                console.log('newPlotDivWidth:  ' + newPlotDivWidth);
            }

            $('#plotCanvasDiv').height(newPlotDivHeight);
            DATA_DISPLAY.plotWidth = newPlotDivWidth;
            DATA_DISPLAY.plotHeight = newPlotDivHeight;

            if (debug) {
                console.debug('***  END  data-display.js calculatePlotSize');
            }
        },


        // Plot the data!
        plotLine : function (values, layout) {

            DATA_DISPLAY.displayType = 'line';

            DATA_DISPLAY.showPlotCanvas();

            DATA_DISPLAY.calculatePlotSize();

            DATA_DISPLAY.lineYValues = values;
            DATA_DISPLAY.lineTitle = layout.title;

            DATA_DISPLAY.drawLine(null, values, layout);

        },


        // Plot the data! This redraws everything
        plotImageData : function () {

            var debug = false;

            DATA_DISPLAY.displayType = 'image';
            DATA_DISPLAY.showPlotCanvas();
            DATA_DISPLAY.calculatePlotSize();

            if (debug) {
                console.log('DATA_DISPLAY.plotDimension: ' +
                    DATA_DISPLAY.plotDimension);
            }

            if (DATA_DISPLAY.plotDimension === 2) {
                DATA_DISPLAY.draw2DPlot();
            } else {
                DATA_DISPLAY.draw3DPlot();
            }

        },


        // Change the data used in the plot without redrawing everything
        updatePlotZData : function (ranges, newImageFetched, setAxesRange) {

            var debug = false, profiles, mainAxisPercent;

            if (debug) {
                console.log('** updatePlotZData **');
                console.log('refilling projections');
                console.log('ranges:');
                console.log(ranges);
                console.log('newImageFetched: ' + newImageFetched);
            }

            if (!ranges) {
                if (DATA_DISPLAY.imageZoomSection) {
                    ranges = DATA_DISPLAY.imageZoomSection;
                } else {
                    ranges = DATA_DISPLAY.loadedImageRange;
                }
            }

            // Refill the profile projections
            profiles = DATA_DISPLAY.fillProfileProjections(ranges);

            if (debug) {
                console.log('profiles:');
                console.log(profiles);
            }

            if (newImageFetched) {

                // Refill the 2D or 3D plot, set the min and max so the color
                // bar range updates
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    z: [DATA_DISPLAY.dataValues],
                    text: [DATA_DISPLAY.stringDataValues],
                    x: [profiles.imageXAxis],
                    y: [profiles.imageYAxis],
                    zmin: [profiles.zMin],
                    zmax: [profiles.zMax],
                    colorscale: [DATA_DISPLAY.colorScale],
                    colorbar : [{
                        "title" : (DATA_DISPLAY.plotLogValues ? "10^" : ""),
                        "titleside" : "bottom",
                        "exponentformat" : "power",
                        "titlefont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#DDDDCE" : "#000000"),
                        },
                        "tickfont" : {
                            "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                    "#999" : "#000000"),
                        },
                    }],
                }, [0]);

                // Set the ranges of the 2D plot properly - otherwise empty
                // bands will appears when not centered on data.
                if (DATA_DISPLAY.plotDimension === 2) {
                    if (debug) {
                        console.log('DATA_DISPLAY.imageZoomSection:');
                        console.log(DATA_DISPLAY.imageZoomSection[0]);
                        console.log(DATA_DISPLAY.imageZoomSection[1]);
                        console.log(DATA_DISPLAY.imageZoomSection[2]);
                        console.log(DATA_DISPLAY.imageZoomSection[3]);
                        console.log('DATA_DISPLAY.plotAspect: ' +
                            DATA_DISPLAY.plotAspect);
                    }

                    if (setAxesRange) {
                        console.log('setting axes ranges - doing another' +
                            ' relayout');

                        // Also, the domain needs to be set again, not sure
                        // why...
                        mainAxisPercent = DATA_DISPLAY.plotProjection
                            === true ? 0.85 : 1;
                        Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                            "xaxis" : {
                                "autorange" : true,
                                "domain" : [0, mainAxisPercent]
                            },

                            "yaxis": {
                                "autorange" : true,
                                "domain" : [0, mainAxisPercent],

                                "scaleanchor" : ["x"],
                                "scaleratio" : [1],
                                "constraintoward" : ["top"],
                            },
                        });
                    }
                }

                // If an image series, change the title
                Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                    "title" : DATA_DISPLAY.mobileDisplay === true
                              ? ''
                              : DATA_DISPLAY.generateImageTitle(profiles.zMin,
                                  profiles.zMax)
                });


            } else {
                // Set the min and max so the color bar range updates
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    zmin: [profiles.zMin],
                    zmax: [profiles.zMax],
                }, [0]);
            }

            if (DATA_DISPLAY.plotDimension === 2 &&
                    DATA_DISPLAY.plotProjection) {
                // Update the profile projections in the plot
                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    x: [DATA_DISPLAY.dataProjectionValues.xx],
                    y: [DATA_DISPLAY.dataProjectionValues.xy],
                    marker: [{color: DATA_DISPLAY.colorProfile}],
                }, [1]);

                Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                    x: [DATA_DISPLAY.dataProjectionValues.yy],
                    y: [DATA_DISPLAY.dataProjectionValues.yx],
                    marker: [{color: DATA_DISPLAY.colorProfile}],
                }, [2]);
            }

            AJAX_SPINNER.doneLoadingData();

            if (debug) {
                console.log('** End of updatePlotZData **');
            }
        },


        // Change the plot type - 2D 'heatmap' or 3D 'surface' seem to be the
        // best options
        changeType : function (type) {

            if (type !== '') {

                // Toggle which item in the list in highlighted
                $("#plotType" +
                    DATA_DISPLAY.plotType).removeClass('selected');
                $("#plotType" + type).addClass('selected');
                $("#plotType" +
                    DATA_DISPLAY.plotType + "Mobile").removeClass('selected');
                $("#plotType" + type + "Mobile").addClass('selected');


                // Save the new plot type
                DATA_DISPLAY.plotType = type;

                if (DATA_DISPLAY.plotType === 'heatmap') {
                    DATA_DISPLAY.plotDimension = 2;
                } else {
                    DATA_DISPLAY.plotDimension = 3;
                }

                DATA_DISPLAY.purgePlotCanvas();
                AJAX_SPINNER.startLoadingData(1);

                // Use a bit of a delay just so that the loading spinner has
                // a chance to start up
                setTimeout(function () {
                    DATA_DISPLAY.plotImageData();
                }, 300);
            }

        },


        // Switch between the use of log and non-log values
        toggleAspect : function () {

            var debug = false;

            if (DATA_DISPLAY.plotAspect) {
                DATA_DISPLAY.plotAspect = false;
                $("#aspectTooltip").html('Aspect ratio is not kept');
                $('#aspectIcon').addClass('hidden');
                $('#noAspectIcon').removeClass('hidden');
            } else {
                DATA_DISPLAY.plotAspect = true;
                $("#aspectTooltip").html('Aspect ratio is kept');
                $('#aspectIcon').removeClass('hidden');
                $('#noAspectIcon').addClass('hidden');
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotAspect: ' +
                    DATA_DISPLAY.plotAspect);
            }

            DATA_DISPLAY.purgePlotCanvas();
            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has
            // a chance to start up
            setTimeout(function () {
                DATA_DISPLAY.plotImageData();
            }, 300);
        },


        // Switch between the use of log and non-log values
        toggleType : function () {

            var debug = false;

            if (DATA_DISPLAY.plotDimension === 2) {

                DATA_DISPLAY.plotDimension = 3;
                DATA_DISPLAY.plotType = 'surface';
                $('#plotOption3D .fa-check').css('visibility', 'visible');
                $('#plotOptionProjection').addClass('disabled');
                $('#plotControlAspect').addClass('hidden');

            } else {

                DATA_DISPLAY.plotDimension = 2;
                DATA_DISPLAY.plotType = 'heatmap';

                $('#plotOption3D .fa-check').css('visibility', 'hidden');
                $('#plotOptionProjection').removeClass('disabled');
                $('#plotControlAspect').removeClass('hidden');

            }

            if (debug) {
                console.log('DATA_DISPLAY.plotDimension: ' +
                    DATA_DISPLAY.plotDimension);
                console.log('DATA_DISPLAY.plotType: ' + DATA_DISPLAY.plotType);
            }

            DATA_DISPLAY.purgePlotCanvas();
            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has
            // a chance to start up
            setTimeout(function () {
                DATA_DISPLAY.plotImageData();
            }, 300);
        },


        // Change the color map
        changeColor : function (colorScale) {

            var debug = false;

            if (debug) {
                console.log('colorScale: ' + colorScale);
            }

            if (colorScale !== '') {

                // Toggle which item in the menu is highlighted
                $('#' + DATA_DISPLAY.colorScale + 'Icon').addClass('hidden');
                $("#plotColor" +
                    DATA_DISPLAY.colorScale).removeClass('selected');
                $("#plotColor" + DATA_DISPLAY.colorScale +
                    "Mobile").removeClass('selected');

                $("#plotColor" +
                    colorScale).addClass('selected');
                $("#plotColor" + colorScale +
                    "Mobile").addClass('selected');

                // Save new color choice
                DATA_DISPLAY.colorScale = colorScale;

                // Set the tooltip
                // $("#colormapTooltip").html('Colormap: ' + colorScale);

                // Set the 2D profile projection color
                switch (colorScale) {
                case 'Electric':
                    DATA_DISPLAY.colorProfile = 'rgb(204,0,153)';
                    break;
                case 'Greys':
                    DATA_DISPLAY.colorProfile = 'rgb(115,115,115)';
                    break;
                case 'Hot':
                    DATA_DISPLAY.colorProfile = 'rgb(153,0,0)';
                    break;
                case 'Jet':
                    DATA_DISPLAY.colorProfile = 'rgb(0,153,255)';
                    break;
                case 'Picnic':
                    DATA_DISPLAY.colorProfile = 'rgb(255,153,221)';
                    break;
                default:
                    DATA_DISPLAY.colorProfile = 'rgb(102,0,0)';
                }

                // And the sweet little icon
                $('#' + colorScale + 'Icon').removeClass('hidden');

                if (debug) {
                    console.log('DATA_DISPLAY.colorScale:');
                    console.log(DATA_DISPLAY.colorScale);
                    console.log('DATA_DISPLAY.colorProfile: ' +
                        DATA_DISPLAY.colorProfile);
                }

                // Change the plot color using a restyle event
                if (DATA_DISPLAY.plotExists) {
                    Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                        colorscale: DATA_DISPLAY.colorScale
                    }, [0]);

                    // Update the profile projections in the plot if we plot
                    // projection and it's not just text
                    if (DATA_DISPLAY.plotDimension === 2
                            && DATA_DISPLAY.plotProjection
                            && DATA_DISPLAY.displayType !== 'text'
                            && !DATA_DISPLAY.plotHistogram) {
                        Plotly.restyle(DATA_DISPLAY.plotCanvasDiv, {
                            marker: [{color: DATA_DISPLAY.colorProfile}],
                        }, [1, 2]);
                    }
                }
            }
        },


        // One of the plotly buttons remade - reset zoom / autoscale
        resetZoom : function () {

            var debug = false, layout;

            if (debug) {
                console.log();
            }

            // Start the spinner
            AJAX_SPINNER.startLoadingData(1);

            // The simplest way to reset the zoom seems to be to just replot
            // everything

            // For an image series
            if (DATA_DISPLAY.imageSeries &&
                    !DATA_DISPLAY.plotHistogram) {

                HANDLE_DATASET.imageSeriesInput(DATA_DISPLAY.imageSeriesIndex,
                    false);

            // For an image
            } else if (DATA_DISPLAY.displayType === 'image') {

                HANDLE_DATASET.requestForData(DATA_DISPLAY.dataPath, false,
                    true, false);

            // For a line
            } else if (DATA_DISPLAY.displayType === 'line') {

                layout = {
                    "title" : DATA_DISPLAY.lineTitle,
                    "xLabel" : "Array Index",
                    "yLabel" : "Values"
                };

                DATA_DISPLAY.drawLine(DATA_DISPLAY.lineXValues,
                    DATA_DISPLAY.lineYValues, layout);

            } else if (DATA_DISPLAY.displayType === 'bar') {

                layout = {
                    "title" : 'Intensity histogram',
                    "xLabel" : DATA_DISPLAY.plotLogXValues ?
                               'Log(Intensity)' :
                               'Intensity',
                    "yLabel" : DATA_DISPLAY.plotLogYValues ?
                               'Log(No. Points)' :
                               'No. Points'
                };

                DATA_DISPLAY.drawHistogram(layout);
            }
        },


        // One of the plotly buttons remade - download the plot image
        downloadPlot : function () {

            // Get the plot dimensions
            var debug = false, divWidth = $('#plotCanvasDiv').width(),
                divHeight = $('#plotCanvasDiv').height();

            // Start the spinner
            AJAX_SPINNER.startLoadingData(1);

            // Download that shit! Wait a bit so that the loader is visible :)
            setTimeout(function () {

                if (debug) {
                    console.log('starting plotly download function');
                }

                Plotly.downloadImage(
                    DATA_DISPLAY.plotCanvasDiv,
                    {
                        format: 'png',
                        width: divWidth,
                        height: divHeight,
                        filename: 'newplot'
                    }
                ).then(
                    // There seems to be a delay between when plotly is done
                    // creating 3D images, and when the download dialog appears
                    AJAX_SPINNER.doneLoadingData(1000)
                );
            }, 50);
        },


        // One of the plotly buttons remade - download the plot image
        sharePlot : function () {
            var plotURL, dataPath, emailSubject, emailBody, shareModal,
                shareModalContent, closeModalBtn, clickedInputTitle,
                clickedInput;

            // Save the modal as a variable and show it
            shareModal = $('#shareModal');
            shareModal.removeClass('hidden');

            // Addtional class for color theme
            shareModalContent = shareModal.find('.modal-content');
            shareModalContent.toggleClass('darkTheme',
                DATA_DISPLAY.useDarkTheme);

            // Copyable links (URL and data path)
            plotURL = decodeURIComponent(window.location.href);
            dataPath = plotURL.split('data=')[1];

            // Add URL and data path to the inputs
            shareModal.find('#shareURL input')[0].value = plotURL;
            shareModal.find('#shareDataPath input')[0].value = dataPath;

            // Email content ("%0A" is a new line)
            emailSubject = "Data plot";
            emailBody = "Hello! %0A%0A" +
                        "Here is the link to my data plot: %0A" +
                        encodeURIComponent(plotURL) + "%0A%0A" +
                        "Cheers,%0A" +
                        CAS_TICKET.firstName;

            // Apply correct mailto link with the email content
            shareModal.find('.share-mail-btn')[0].href = "mailto:" +
                "?subject=" + emailSubject +
                "&body=" + emailBody;


            // Copy to clipboard click events
            shareModal.find('#shareURL a, #shareDataPath a').on('click',
                function (e) {
                    // Save the clicked input title
                    clickedInputTitle =
                        e.target.parentNode.previousElementSibling;

                    // Check if input title contains "copied". If not
                    // add it and reset the input title that wasn't clicked
                    if (!clickedInputTitle.innerText.includes('copied')) {
                        clickedInputTitle.innerText =
                            clickedInputTitle.innerText + " (copied)";

                        if (clickedInputTitle.innerText.includes('URL')) {
                            shareModal.find(
                                '#shareDataPath .modal-input-title'
                            )[0].innerText = "Data path:";
                        } else {
                            shareModal.find(
                                '#shareURL .modal-input-title'
                            )[0].innerText = "URL:";
                        }
                    }

                    // Copy the text from the input (URL or data path)
                    clickedInput = e.target.previousElementSibling;
                    clickedInput.select();
                    document.execCommand('copy');
                }
                );

            // Close modal on click outside modal or close button
            shareModal.on('click', function (e) {
                closeModalBtn = shareModal.find('i.fa-times')[0];
                if (e.target === shareModal[0] || e.target === closeModalBtn) {
                    shareModal.addClass('hidden');
                }
            });
        },


        // Switch between the use of automatic image manipulation or not
        toggleAutoImage : function (useAutoLimits) {

            var debug = false;

            if (debug) {
                console.log('useAutoLimits: ' + useAutoLimits);
            }

            if (useAutoLimits === undefined) {
                DATA_DISPLAY.plotAutoLimits = !DATA_DISPLAY.plotAutoLimits;
            } else {
                DATA_DISPLAY.plotAutoLimits = useAutoLimits;
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotAutoLimits: ' +
                    DATA_DISPLAY.plotAutoLimits);
            }

            // Change the color of the button and the text displayed
            if (DATA_DISPLAY.plotAutoLimits) {
                if (debug) {
                    console.log('Automatic intensity limits in use');
                }
                $("#autoImageButton").html(
                    'Automatic intensity limits in use'
                );
                $('#autoLimitsIcon').removeClass('hidden');
                $('#noAutoLimitsIcon').addClass('hidden');
            } else {
                if (debug) {
                    console.log('Automatic Limits?');
                }
                $("#autoImageButton").html(
                    'Automatic intensity limits not in use'
                );
                $('#autoLimitsIcon').addClass('hidden');
                $('#noAutoLimitsIcon').removeClass('hidden');
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has a
            // chance to start up and the plot image has time to change
            setTimeout(function () {
                DATA_DISPLAY.updatePlotZData(false, true, false);
            }, 300);
        },



        // Switch between the use of log and non-log values
        toggleLogPlot : function (xLogToggle, yLogToggle, useLog) {

            var debug = false;

            if (debug) {
                console.log('xLogToggle: ' + xLogToggle);
                console.log('yLogToggle: ' + yLogToggle);
                console.log('useLog: ' + useLog);
            }

            if (xLogToggle === undefined && yLogToggle === undefined) {
                if (useLog === undefined) {
                    DATA_DISPLAY.plotLogValues = !DATA_DISPLAY.plotLogValues;
                } else {
                    DATA_DISPLAY.plotLogValues = useLog;
                }

                if (debug) {
                    console.log('DATA_DISPLAY.plotLogValues: ' +
                        DATA_DISPLAY.plotLogValues);
                }

                // Change the data to be displayed
                if (DATA_DISPLAY.plotLogValues) {
                    if (debug) {
                        console.log('Log Plot!');
                    }

                    $('#plotOptionLog .fa-check').css('visibility', 'visible');

                } else {
                    if (debug) {
                        console.log('Log Plot?');
                    }

                    $('#plotOptionLog .fa-check').css('visibility', 'hidden');
                }
            } else {
                if (xLogToggle) {
                    DATA_DISPLAY.plotLogXValues =
                        !DATA_DISPLAY.plotLogXValues;
                }

                if (yLogToggle) {
                    DATA_DISPLAY.plotLogYValues =
                        !DATA_DISPLAY.plotLogYValues;
                }
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            AJAX_SPINNER.startLoadingData(1);

            // Use a bit of a delay just so that the loading spinner has a
            // chance to start up and the log button background has time to
            // change color :p
            setTimeout(function () {
                // Check if the log toggle was triggerd in histogram or not
                if (DATA_DISPLAY.plotHistogram) {
                    DATA_DISPLAY.toggleHistogram(true);
                } else {
                    DATA_DISPLAY.updatePlotZData(false, true, false);
                }
            }, 300);
        },


        // Toggle the display of axis projections
        toggleProjectionPlot : function (useProjection) {

            var debug = false, projectionData, xProfilePlot, yProfilePlot;

            if (debug) {
                console.log('useProjection: ' + useProjection);
            }

            if (useProjection === undefined) {
                DATA_DISPLAY.plotProjection = !DATA_DISPLAY.plotProjection;
            } else {
                DATA_DISPLAY.plotProjection = useProjection;
            }

            if (debug) {
                console.log('DATA_DISPLAY.plotProjection: ' +
                    DATA_DISPLAY.plotProjection);
            }

            // Change the data to be displayed
            if (DATA_DISPLAY.plotProjection) {
                if (debug) {
                    console.log('Projection Plot!');
                }

                $('#plotOptionProjection .fa-check').css('visibility',
                    'visible');

                AJAX_SPINNER.startLoadingData(1);

                // Check if we have any calculated projection data, if not we
                // calculate it
                projectionData = DATA_DISPLAY.dataProjectionValues;
                if ($.isEmptyObject(projectionData.xx, projectionData.xy,
                        projectionData.yx, projectionData.yy)) {
                    DATA_DISPLAY.fillProfileProjections(
                        DATA_DISPLAY.imageZoomSection
                    );
                }

                // The x-profile of the plot, displayed as a bar chart
                xProfilePlot = {
                    x: DATA_DISPLAY.dataProjectionValues.xx,
                    y: DATA_DISPLAY.dataProjectionValues.xy,
                    xmax: 500,
                    ymax: 500,
                    zmax: 500,
                    name: 'x profile',
                    marker: {color: DATA_DISPLAY.colorProfile},
                    yaxis: 'y2',
                    type: 'bar'
                };

                // The y-profile, also displayed as a bar chart,
                // oriented horizontally --> switch x & y
                yProfilePlot = {
                    x: DATA_DISPLAY.dataProjectionValues.yy,
                    y: DATA_DISPLAY.dataProjectionValues.yx,
                    name: 'y profile',
                    marker: {color: DATA_DISPLAY.colorProfile},
                    xaxis: 'x2',
                    type: 'bar',
                    orientation: 'h'
                };


                setTimeout(function () {
                    // Add the newly calculated projection  traces to the data
                    Plotly.addTraces(DATA_DISPLAY.plotCanvasDiv,
                        [xProfilePlot, yProfilePlot]);

                    // Change the axis domains to fit the projections
                    Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                        "xaxis" : {
                            "domain" : [0, 0.85]
                        },

                        "yaxis": {
                            "domain" : [0, 0.85],
                        },

                        "xaxis2": {
                            "domain" : [0.85, 1],

                        },

                        "yaxis2": {
                            "domain" : [0.85, 1],

                        }
                    });

                    AJAX_SPINNER.doneLoadingData();
                }, 300);
            } else {
                if (debug) {
                    console.log('Projection Plot?');
                }

                // $("#logPlotButton").html('Log of intensity not displayed');
                $('#plotOptionProjection .fa-check').css('visibility',
                    'hidden');

                AJAX_SPINNER.startLoadingData(1);

                // Change the main plot's domain to the whole screen and remove
                // the projection traces after a delay so the preloader appears
                setTimeout(function () {
                    Plotly.deleteTraces(DATA_DISPLAY.plotCanvasDiv, [1, 2]);

                    Plotly.relayout(DATA_DISPLAY.plotCanvasDiv, {
                        "xaxis" : {
                            "domain" : [0, 1]
                        },

                        "yaxis": {
                            "domain" : [0, 1],
                        },
                    });

                    AJAX_SPINNER.doneLoadingData();
                }, 300);
            }
        },


        // Create several images using:
        //  - Raw data
        //  - Log of raw data
        //  - Raw data with bad pixels removed
        //  - Log of raw data with bad pixels removed
        createImageArrays : function (value, bpr_limits, badPixelsExist) {

            var debug = false, i, j, logValue;

            if (debug) {
                console.log('*** START data-display.js createImageArrays');
                console.log('bpr_limits: ');
                console.log(bpr_limits);
                console.log('badPixelsExist: ' + badPixelsExist);
            }

            // Array containing raw data
            DATA_DISPLAY.initialDataValues = value;

            // The log of the raw data
            DATA_DISPLAY.logOfDataValues = [];

            // The raw data in string form
            DATA_DISPLAY.stringDataValues = [];

            // The intensity histogram's data
            DATA_DISPLAY.histogramValues = {};
            DATA_DISPLAY.histogramLogValues = {};

            // Any bad pixels?
            DATA_DISPLAY.badPixelsExist = badPixelsExist;

            // Arrays containing data with bad pixels removed (BPR)
            if (badPixelsExist) {
                DATA_DISPLAY.initialDataValuesBPR = [];
                DATA_DISPLAY.logOfDataValuesBPR = [];
            }

            // Show or hide the automatic intensity limits button
            if (badPixelsExist) {
                $('#plotControlAutoImage').show();
            } else {
                $('#plotControlAutoImage').hide();
            }

            // Take the log of the points, save for future use
            for (i = 0; i < value.length; i += 1) {

                DATA_DISPLAY.logOfDataValues[i] = [];
                DATA_DISPLAY.stringDataValues[i] = [];
                if (badPixelsExist) {
                    DATA_DISPLAY.initialDataValuesBPR[i] = [];
                    DATA_DISPLAY.logOfDataValuesBPR[i] = [];
                }

                for (j = 0; j < value[i].length; j += 1) {

                    // If there are any bad pixels, remove them from the image
                    // by setting their values to null
                    if (badPixelsExist) {
                        if (value[i][j] >= bpr_limits[0] &&
                                value[i][j] <= bpr_limits[1]) {

                            DATA_DISPLAY.initialDataValuesBPR[i][j] =
                                value[i][j];

                            DATA_DISPLAY.histogramValues[value[i][j]] = 1 +
                              (DATA_DISPLAY.histogramValues[value[i][j]] || 0);

                            // Calculate the log of each value
                            if (value[i][j] > 0) {
                                logValue = Math.log(value[i][j]) / Math.LN10;
                                DATA_DISPLAY.logOfDataValuesBPR[i][j] =
                                    logValue;

                                DATA_DISPLAY.histogramLogValues[logValue] = 1 +
                                (DATA_DISPLAY.histogramLogValues[logValue] || 0);
                            } else {
                                // Set log of <= 0 values to null - I think it
                                // looks nicer when plotted
                                DATA_DISPLAY.logOfDataValuesBPR[i][j] = null;
                            }

                        } else {
                            DATA_DISPLAY.initialDataValuesBPR[i][j] = null;
                            DATA_DISPLAY.logOfDataValuesBPR[i][j] = null;
                        }
                    }

                    // Calculate the log of each value
                    if (value[i][j] > 0) {

                        DATA_DISPLAY.logOfDataValues[i][j] =
                            Math.log(value[i][j]) / Math.LN10;

                    } else {
                        // Set log of <= 0 values to null - I think it
                        // looks nicer when plotted
                        DATA_DISPLAY.logOfDataValues[i][j] = null;
                    }

                    // Convert to string for use in the mouse-over tool-tip,
                    // otherwise values of '0' are not displayed and I also
                    // want to have the tool-tip show out of range values even
                    // if they are not displayed on the plot
                    DATA_DISPLAY.stringDataValues[i][j] =
                        value[i][j].toString();
                }
            }

            // Set the data that will be used for plotting
            DATA_DISPLAY.setDataToDisplay();

            if (debug) {
                console.log('*** END data-display.js createImageArrays');
            }
        },

        // Set the data that will be used for plotting - log or not, automatic
        // limits or not
        setDataToDisplay : function () {

            if (DATA_DISPLAY.plotLogValues) {

                if (DATA_DISPLAY.plotAutoLimits &&
                        DATA_DISPLAY.badPixelsExist) {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.logOfDataValuesBPR;
                } else {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.logOfDataValues;
                }

            } else {

                if (DATA_DISPLAY.plotAutoLimits &&
                        DATA_DISPLAY.badPixelsExist) {
                    DATA_DISPLAY.dataValues =
                        DATA_DISPLAY.initialDataValuesBPR;
                } else {
                    DATA_DISPLAY.dataValues = DATA_DISPLAY.initialDataValues;
                }

            }
        },


        // Save the image data and the log of the image data to global
        // variables (2D heatmaps have no option to switch the z-axis to log
        // scale!)
        //
        //  - raw image size : shape dims
        //  - loaded image size : width and height in number of pixels
        //  - loaded image range : of the raw image
        //  - zoom range
        //
        //------------------------------------------------
        //  - original data image size (data taken)
        //  - selected range of image (zoom)
        //  - size of returned image (could be downsampled or not)
        //------------------------------------------------
        //
        initializeImageData : function (value, bpr_limits, badPixels,
            is_downsampled) {

            var debug = false;

            DATA_DISPLAY.dataValues = value;

            // The size (in pixels) of the image
            DATA_DISPLAY.loadedImageSize = [DATA_DISPLAY.dataValues[0].length,
                DATA_DISPLAY.dataValues.length];

            // The range of the image
            //      → should be 0 to something, unless zoomed in on a
            //        previously downsampled image
            if (DATA_DISPLAY.imageZoomSection) {
                DATA_DISPLAY.loadedImageRange =
                    DATA_DISPLAY.imageZoomSection;
            } else {
                DATA_DISPLAY.loadedImageRange =
                    [0, DATA_DISPLAY.imageShapeDims[0],
                        0, DATA_DISPLAY.imageShapeDims[1]];
            }

            if (!DATA_DISPLAY.imageZoomSection) {
                DATA_DISPLAY.imageZoomSection = DATA_DISPLAY.loadedImageRange;
            }

            // The size of the image range
            DATA_DISPLAY.loadedImageRangeSize[0] =
                DATA_DISPLAY.loadedImageRange[1] -
                DATA_DISPLAY.loadedImageRange[0];
            DATA_DISPLAY.loadedImageRangeSize[1] =
                DATA_DISPLAY.loadedImageRange[3] -
                DATA_DISPLAY.loadedImageRange[2];

            // Check if the image has been downsampled
            DATA_DISPLAY.imageIsDownsampled = is_downsampled;

            if (debug) {
                console.log('DATA_DISPLAY.imageShapeDims[0]' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]' +
                    DATA_DISPLAY.imageShapeDims[1]);
                console.log('DATA_DISPLAY.loadedImageSize: ' +
                    DATA_DISPLAY.loadedImageSize);
                console.log('DATA_DISPLAY.loadedImageRange: ' +
                    DATA_DISPLAY.loadedImageRange);
                console.log('DATA_DISPLAY.loadedImageRangeSize: ' +
                    DATA_DISPLAY.loadedImageRangeSize);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
                console.log('DATA_DISPLAY.imageIsDownsampled: ' +
                    DATA_DISPLAY.imageIsDownsampled);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
            }

            // Create the image arrays
            DATA_DISPLAY.createImageArrays(value, bpr_limits, badPixels);

            if (debug) {
                console.log('DATA_DISPLAY.initialDataValues:');
                console.log(DATA_DISPLAY.initialDataValues);
            }
        },


        // Check if the object is a number
        isNumeric : function (n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },

        // Check if odd or even
        isEven : function (n) {

            var result = (n % 2 === 0) ? true : false;
            return result;
        },

        // Save some information about an image
        //
        // image
        //      - new image
        //      - decimated image
        //      - zoomed
        //          - fetched zoomed area
        //          -
        //
        // image series
        //      - new series
        //      - new image in same series
        //          - previous image was zoomed
        //      - zoomed
        //          - fetched zoomed area
        //
        //------------------------------------------------
        //  - original data image size (data taken)
        //  - selected range of image (zoom)
        //  - size of returned image (could be downsampled or not)
        //------------------------------------------------
        //
        saveImageInfo : function (targetUrl, shapeDims, newImage, section,
            imageTitle, dataPath) {

            var debug = false;

            // Save some data, if provided
            if (targetUrl) {
                // DATA_DISPLAY.imageTargetUrl = targetUrl;
                DATA_DISPLAY.imageTargetUrl = targetUrl.split('[')[0];
                DATA_DISPLAY.dataPath = dataPath.split('[')[0];
            }

            if (shapeDims) {

                // image-series
                if (shapeDims.length === 3) {
                    DATA_DISPLAY.imageSeriesRange = shapeDims[0];

                    // Switch (row,col) notation to (x,y) notation
                    DATA_DISPLAY.imageShapeDims = [shapeDims[2], shapeDims[1]];

                    if (section) {
                        DATA_DISPLAY.imageSeriesIndex = section[0];
                    } else {
                        DATA_DISPLAY.imageSeriesIndex = 0;
                    }

                // image
                } else {
                    DATA_DISPLAY.imageSeriesRange = 1;
                    DATA_DISPLAY.imageSeriesIndex = false;

                    // Switch (row,col) notation to (x,y) notation
                    DATA_DISPLAY.imageShapeDims = [shapeDims[1], shapeDims[0]];
                }
            }

            DATA_DISPLAY.usingOriginalImage = newImage;

            // Save zooming info
            if (section !== true) {
                DATA_DISPLAY.imageZoomSection = section;
            }

            if (section) {

                // image-series
                if (shapeDims.length === 3) {
                    DATA_DISPLAY.imageZoomSection = [];
                    DATA_DISPLAY.imageZoomSection[0] = section[2];
                    DATA_DISPLAY.imageZoomSection[1] = section[3];
                    DATA_DISPLAY.imageZoomSection[2] = section[4];
                    DATA_DISPLAY.imageZoomSection[3] = section[5];

                // image
                } else {
                    DATA_DISPLAY.imageZoomSection = section;
                }
            }


            // Set the plot title
            if (imageTitle) {
                DATA_DISPLAY.imageTitle = imageTitle;
            }

            if (debug) {
                console.log('DATA_DISPLAY.imageTitle: ' +
                    DATA_DISPLAY.imageTitle);
                console.log('DATA_DISPLAY.imageSeriesIndex: ' +
                    DATA_DISPLAY.imageSeriesIndex);
                console.log('DATA_DISPLAY.imageSeriesRange: ' +
                    DATA_DISPLAY.imageSeriesRange);
                console.log('DATA_DISPLAY.imageShapeDims[0]: ' +
                    DATA_DISPLAY.imageShapeDims[0]);
                console.log('DATA_DISPLAY.imageShapeDims[1]: ' +
                    DATA_DISPLAY.imageShapeDims[1]);
                console.log('DATA_DISPLAY.imageZoomSection:   ' +
                    DATA_DISPLAY.imageZoomSection);
                console.log('DATA_DISPLAY.usingOriginalImage: ' +
                    DATA_DISPLAY.usingOriginalImage);
                console.log('** DATA_DISPLAY.saveImageInfo done **');
            }

        },

        // Reload the page
        reloadPage : function () {

            var debug = false, serviceUrl;

            // Get the bare application service url from the confing file
            serviceUrl = window.location.origin + CONFIG_DATA.servicePath;

            if (debug) {
                console.debug('** Reloading page, opening serviceUrl: ' +
                    serviceUrl);
            }

            // Open the url
            window.location = serviceUrl;
        },

        // Plot the intensity histogram
        toggleHistogram : function (showHistogram) {
            var debug = false, intensityCounts, intensities,
                histogramData, sortedIndex, layout, intensity,
                intensityCount, intensityMin, intensityMax,
                intensityBinSize, intensityBinCount, xBins;

            if (showHistogram === undefined) {
                DATA_DISPLAY.plotHistogram =
                    !DATA_DISPLAY.plotHistogram;
            } else {
                DATA_DISPLAY.plotHistogram = showHistogram;
            }

            if (debug) {
                console.log('Show intensity histogram:',
                    DATA_DISPLAY.plotHistogram);
            }

            // Show the intensity histogram plot
            if (DATA_DISPLAY.plotHistogram) {

                if (debug) {
                    console.log('Showing the intensity histogram...');
                    console.log('Log of intensities',
                        DATA_DISPLAY.plotLogXValues);
                    console.log('Log of counts', DATA_DISPLAY.plotLogYValues);
                }

                // Open preloader
                AJAX_SPINNER.startLoadingData(1);

                // Save the current image histogram data
                if (DATA_DISPLAY.plotLogXValues) {
                    histogramData = DATA_DISPLAY.histogramLogValues;
                } else {
                    histogramData = DATA_DISPLAY.histogramValues;
                }

                // Set the intensity bin variables (calculated below)
                intensityBinCount = 500;
                intensityMax = -1e100;
                intensityMin = 1e100;

                // Seperate histogramData into two arrays of counts
                // respectively intensity
                intensityCounts = [];
                intensities = [];
                for (intensity in histogramData) {
                    // Add the current intensity to the intensities arr and
                    // sort it
                    intensity = parseFloat(intensity);
                    intensities.push(intensity);
                    intensities.sort(function (a, b) {
                        return a - b;
                    });

                    // Get the sorted index of the current intensity and add
                    // its count to the intensityCounts in the same place
                    sortedIndex = intensities.indexOf(intensity);
                    intensityCount = DATA_DISPLAY.plotLogYValues ?
                            Math.log(histogramData[intensity]) :
                            histogramData[intensity];
                    intensityCounts.splice(sortedIndex, 0, intensityCount);

                    // Calculate the min and max intensities
                    if (intensity < intensityMin && intensityCount !== 0) {
                        intensityMin = intensity;
                    }

                    if (intensity > intensityMax && intensityCount !== 0) {
                        intensityMax = intensity;
                    }
                }

                // Set the intensity bin size
                intensityBinSize =
                    (intensityMax - intensityMin) / intensityBinCount;

                // Set the correct xBin variable to match with plotly
                xBins = {
                    "start" : intensityMin,
                    "end" : intensityMax,
                    "size" : intensityBinSize,
                };

                // Prepare plotly layout
                layout = {
                    "title" : 'Intensity histogram',
                    "xLabel" : DATA_DISPLAY.plotLogXValues ?
                               'Log(Intensity)' :
                               'Intensity',
                    "yLabel" : DATA_DISPLAY.plotLogYValues ?
                               'Log(No. Points)' :
                               'No. Points'
                };

                // Save the calculated data and information about the plot
                DATA_DISPLAY.displayType = 'bar';
                DATA_DISPLAY.barYValues = intensityCounts;
                DATA_DISPLAY.barXValues = intensities;
                DATA_DISPLAY.barXBins = xBins;

                // Wait a bit for the preloader to show
                setTimeout(function () {

                    // Remove unusable buttons in the nav bar
                    DATA_DISPLAY.enableImagePlotControls(true, false,
                        false, true);

                    // Show the check mark icon for active plot options
                    $('#plotOptionHistogram .fa-check').css('visibility',
                        'visible');
                    $('#plotOptionLogX .fa-check').css('visibility',
                        DATA_DISPLAY.plotLogXValues ? 'visible' : 'hidden');
                    $('#plotOptionLogY .fa-check').css('visibility',
                        DATA_DISPLAY.plotLogYValues ? 'visible' : 'hidden');

                    // Disable the unusable options
                    $('#plotOption3D, #plotOptionProjection').addClass(
                        'disabled'
                    );

                    // Show x and y log plot options
                    $('#plotOptionLogX, #plotOptionLogY').removeClass(
                        'hidden'
                    );

                    // Draw the bar plot
                    DATA_DISPLAY.drawHistogram(layout);

                }, 300);

                if (debug) {
                    console.log('Min intenisty:', intensityMin);
                    console.log('Max intenisty:', intensityMax);
                    console.log('Intensity bin size', intensityBinSize);
                    console.log('Counts per unique intensity',
                        histogramData);
                    console.log('Intensities (x-values)', intensities);
                    console.log('Intesity counts (y-values)', intensityCounts);
                }

            // Show the original plot
            } else {
                if (debug) {
                    console.log('Showing the original plot...');
                }

                // Open preloader
                AJAX_SPINNER.startLoadingData(1);

                // Wait a bit for the preloader to show
                setTimeout(function () {

                    // Add usable buttons in the nav bar
                    DATA_DISPLAY.enableImagePlotControls(true, true,
                        DATA_DISPLAY.imageSeries);

                    // Remove the histogram and show original image
                    DATA_DISPLAY.hideHistogram(true);

                    // Close preloader
                    AJAX_SPINNER.doneLoadingData();
                }, 300);
            }
        },

        // Remove check marks, unusable plot options and reset vars
        // and plot original image if draw
        hideHistogram : function (draw) {
            // Reset variables
            DATA_DISPLAY.plotHistogram = false;
            DATA_DISPLAY.plotLogXValues = false;
            DATA_DISPLAY.plotLogYValues = false;
            DATA_DISPLAY.displayType = 'image';
            DATA_DISPLAY.barXValues = [];
            DATA_DISPLAY.barYValues = [];
            DATA_DISPLAY.barXBins = {};

            // Hide the check mark icon for inactive plot options
            $('#plotOptionHistogram .fa-check').css('visibility',
                'hidden');
            $('#plotOptionLogX .fa-check').css('visibility',
                'hidden');
            $('#plotOptionLogY .fa-check').css('visibility',
                'hidden');

            // Enable the usable options
            $('#plotOption3D, #plotOptionProjection').removeClass('disabled');

            // Hide x and y log + bar plot options
            $('#plotOptionLogX, #plotOptionLogY').addClass('hidden');

            // Draw the plot for the original image
            if (draw) {
                if (DATA_DISPLAY.plotDimension === 3) {
                    DATA_DISPLAY.draw3DPlot();
                } else {
                    DATA_DISPLAY.draw2DPlot();
                }
            }
        },

        // Draw a histogram plot with saved display data
        drawHistogram : function (layout) {
            var data, fullLayout, options;

            data = [{
                x: DATA_DISPLAY.barXValues,
                y: DATA_DISPLAY.barYValues,
                type: 'histogram',
                histfunc: 'sum',
                marker: {color: DATA_DISPLAY.colorProfile},
                xbins: DATA_DISPLAY.barXBins
            }];

            fullLayout = {
                "showlegend" : false,
                "title" : (DATA_DISPLAY.mobileDisplay === true ?
                        '' : layout.title),
                "titlefont" : {
                    "color" : (DATA_DISPLAY.useDarkTheme === true ?
                            "#DDDDCE" : "#000000"),
                },
                "autosize" : false,
                "width" : DATA_DISPLAY.plotWidth,
                "height" : DATA_DISPLAY.plotHeight,
                "bargap" : 0,
                "paper_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "plot_bgcolor" : (DATA_DISPLAY.useDarkTheme === true ?
                        '#181817' : '#ffffff'),
                "margin" : {
                    l: 65,
                    r: 50,
                    b: 65,
                    t: 90,
                },
                "xaxis" : {
                    "title" : layout.xLabel,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                },
                "yaxis" : {
                    "title" : layout.yLabel,
                    "titlefont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#DDDDCE" : "#000000"),
                    },
                    "tickfont" : {
                        "color" : (DATA_DISPLAY.useDarkTheme === true ?
                                "#999" : "#000000"),
                    },
                }
            };

            options = {
                staticPlot: false,
                showLink: false,
                displaylogo: false,
                modeBarButtonsToRemove: [
                    'sendDataToCloud', 'hoverCompareCartesian',
                    'hoverClosestCartesian', 'resetScale2d', 'hoverClosest3d',
                    'resetCameraLastSave3d', 'orbitRotation', 'zoomIn2d',
                    'zoomOut2d'],
                displayModeBar: false,
                showTips: false,
                scrollZoom: true,
            };

            DATA_DISPLAY.purgePlotCanvas();
            Plotly.newPlot(DATA_DISPLAY.plotCanvasDiv, data, fullLayout,
                options).then(AJAX_SPINNER.doneLoadingData());
        }
    };


// This function fires when the browser window is resized
$(window).resize(function () {

    var debug = false;

    if (debug) {
        console.log('wait for it...');
    }

    DATA_DISPLAY.mobileDisplay = window.mobilecheck();
    DATA_DISPLAY.redrawPlotCanvas(100);
});


// This function fires when the page is ready
$(document).ready(function () {

    var debug = false;

    if (debug) {
        console.log('document is ready');
    }

});
