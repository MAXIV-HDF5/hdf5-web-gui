/*global $*/
'use strict';

// External libraries
var SERVER_COMMUNICATION, DATA_DISPLAY, FILE_NAV, AJAX_SPINNER, CONFIG_DATA,
    PAGE_LOAD,

    // The global variables for this applicaiton
    HANDLE_DATASET =
    {

        // When image data has been received, examine the reponse from the
        // server, save some information, then display the image
        displayImage : function (response, inputUrl, section, newImage,
            imageTitle) {

            var debug = false, isImageSeries = false, values, shape,
                bpr_limits;

            if (debug) {
                console.debug('** START js/handle-dataset.js displayImage **');
            }

            // DATA_DISPLAY.plotDimension = 2;

            // Hide the 'Reset Zoom' button if this is a new image selected
            // from the tree menu
            if (newImage === true && section === false) {
                $('#plotControlReset').hide();
            }

            if (debug) {
                console.log('HANDLE_DATASET.displayImage response ' +
                    'returned');
                console.log(response);
            }

            // Check that the response is a valid one
            if (response.hasOwnProperty('user_can_read')) {
                if (response.user_can_read) {

                    // Double check that certain items are indeed
                    // arrays, if not, assume that they are strings
                    // containing array notation, and parse them
                    values = response.values;
                    section = response.section;
                    bpr_limits = response.bpr_limits;

                    shape = (response.shape instanceof Array ?
                            response.shape :
                            JSON.parse(response.shape));
                    values = (response.values instanceof Array ?
                            response.values :
                            JSON.parse(response.values));
                    section = (response.section instanceof Array ?
                            response.section :
                            JSON.parse(response.section));
                    bpr_limits = (
                        response.bpr_limits instanceof Array ?
                                response.bpr_limits :
                                JSON.parse(response.bpr_limits)
                    );

                    isImageSeries = (
                        response.item_type === 'image-series' ?
                                true : false
                    );

                    // Switch (row,col) notation to (x,y) notation
                    if (isImageSeries) {
                        section = [section[0], section[1], section[4],
                            section[5], section[2], section[3]];
                    } else {
                        section = [section[2], section[3], section[0],
                            section[1]];
                    }

                    // Save some information about the image
                    DATA_DISPLAY.saveImageInfo(inputUrl,
                        shape, newImage, section, imageTitle, response.path);

                    DATA_DISPLAY.initializeImageData(values, bpr_limits,
                        response.bad_pixels_exist, response.is_downsampled);

                    if (debug) {
                        console.log('HANDLE_DATASET.displayImage newImage: ' +
                            newImage);
                    }

                    // For zooming in a large downsampled image, this should be
                    // false
                    if (newImage) {
                        // Enable plot controls
                        DATA_DISPLAY.enableImagePlotControls(true, true,
                            isImageSeries);
                    }

                    // Plot the data
                    DATA_DISPLAY.plotImageData();

                } else {
                    if (debug) {
                        console.log('object cannot be read');
                    }
                    FILE_NAV.displayErrorMessage(imageTitle,
                        'Object cannot be read!');
                }

            } else {
                if (debug) {
                    console.log('object cannot be read');
                }
                FILE_NAV.displayErrorMessage(imageTitle,
                    'Object cannot be read!');
            }

            if (debug) {
                console.debug('**  END  js/handle-dataset.js displayImage **');
            }

            return true;
        },


        // When a section of an image is requested, add the proper python
        // slicing notation to the path
        prepImagePath : function (dataPath, section) {

            var debug = false;

            if (debug) {
                console.log('** HANDLE_DATASET.prepImagePath entered **');
            }

            // Images are stored in (row, col) notation, which corresponds to
            // (y, x), but zooming in the plotly plots gives coordinates in the
            // (x, y) notation, so they are flipped here in the slice creation
            if (section) {

                // If the data path alread contains slice notation, assume
                // that the input section is the one that is actually wanted
                dataPath = dataPath.split('[')[0];

                // images
                if (section.length === 4) {
                    // axes labels are flipped (I think) in hdf5 datasets
                    dataPath += '[' + section[2] + ':' + section[3] + ','
                        + section[0] + ':' + section[1] + ']';
                }

                // image series
                if (section.length === 6) {
                    // axes labels are flipped (I think) in hdf5 datasets
                    dataPath += '[' + section[0] + ':' + section[1] + ','
                        + section[4] + ':' + section[5] + ',' +
                        section[2] + ':' + section[3] + ']';
                }

            }

            if (debug) {
                console.log('HANDLE_DATASET.prepImagePath section:  ' +
                    section);
                console.log('HANDLE_DATASET.prepImagePath dataPath: ' +
                    dataPath);
            }

            return dataPath;
        },


        // For when an image series step button is pressed
        imageSeriesStep : function (stepUp) {

            var debug = false, imageIndex = 0;

            // Need to add a check to see if an image series is being displayed

            if (debug) {
                console.log('DATA_DISPLAY.imageSeriesIndex: ' +
                    DATA_DISPLAY.imageSeriesIndex);
            }

            if (stepUp) {
                imageIndex = Number(DATA_DISPLAY.imageSeriesIndex) + 1;
            } else {
                imageIndex = Number(DATA_DISPLAY.imageSeriesIndex) - 1;
            }

            if (debug) {
                console.log('imageIndex: ' + imageIndex);
            }

            HANDLE_DATASET.imageSeriesInput(imageIndex);
        },


        // When a particular image series index is selected, check that it is
        // a valid index, use the same zoom section for the next image display,
        // make sure all the image series controls agree with each other, then
        // request and plot the data
        imageSeriesInput : function (imageIndex, zoomSection) {

            var debug = false, section, min = 0,
                max = DATA_DISPLAY.imageSeriesRange - 1;

            if (debug) {
                console.log('** HANDLE-DATASET.imageSeriesInput **');
                console.log('  imageIndex:  ' + imageIndex);
                console.log('  zoomSection: ' + zoomSection);
            }

            // Check if input was numeric - either an actual number or a string
            // that can be aprsed as a number
            if (DATA_DISPLAY.isNumeric(imageIndex)) {

                // Convert to an integer if the value is a string
                imageIndex = parseInt(imageIndex, 10);

            // If the given image index was not numeric, display the first
            // image in the series
            } else {
                imageIndex = 0;
            }

            // Check for out of range values
            if (imageIndex < min) {
                imageIndex = min;
            }

            if (imageIndex > max) {
                imageIndex = max;
            }

            // The zoomed-in section, if applicable
            if (zoomSection === undefined || zoomSection === true) {
                section = [imageIndex, imageIndex + 1,
                    DATA_DISPLAY.imageZoomSection[0],
                    DATA_DISPLAY.imageZoomSection[1],
                    DATA_DISPLAY.imageZoomSection[2],
                    DATA_DISPLAY.imageZoomSection[3]];
            } else if (zoomSection === false) {
                section = false;
                section = [imageIndex, imageIndex + 1,
                    0, DATA_DISPLAY.imageShapeDims[0],
                    0, DATA_DISPLAY.imageShapeDims[1]];
            } else {
                DATA_DISPLAY.imageZoomSection = zoomSection;
                section = [imageIndex, imageIndex + 1,
                    zoomSection[0],
                    zoomSection[1],
                    zoomSection[2],
                    zoomSection[3]];
            }

            if (debug) {
                console.log('  imageIndex: ' + imageIndex);
                console.log('  section: ');
                console.log(section);
                console.log('  min: ' + min);
                console.log('  max: ' + max);
            }

            // Start the spinner
            AJAX_SPINNER.startLoadingData(1);

            // Set image series entry field value
            $("#inputNumberDiv").val(imageIndex);
            $("#imageSeriesSlider").val(imageIndex);

            // Get an image from the series and display it
            HANDLE_DATASET.requestForData(DATA_DISPLAY.dataPath, section,
                true);
        },


        // Show the necessary plotting controls, then plot a line
        displayLine : function (response, imageTitle) {

            var debug = false, layout;

            // Hide the 'Reset Zoom' button, assuming this is a new image
            // selected from the tree menu
            //$('#plotControlReset').hide();

            if (debug) {
                console.log(response);
            }

            // Enable some plot controls
            DATA_DISPLAY.enableImagePlotControls(true, false, false);

            layout = {
                "title" : imageTitle,
                "xLabel" : "Array Index",
                "yLabel" : "Values"
            };
            // Display the data
            DATA_DISPLAY.plotLine(response.values, layout);
        },


        // Request data then decide what to do with it depending on what it is
        // - text, image, etc.
        requestForData : function (dataPath, section, newImage, fromPageLoad) {

            var debug = false, dataUrl = '';

            if (debug) {
                console.debug('*** START js/handle-dataset.js requestForData');
                console.log('  dataPath:     ' + dataPath);
                console.log('  newImage:     ' + newImage);
                console.log('  fromPageLoad: ' + fromPageLoad);
            }

            AJAX_SPINNER.startLoadingData(1);

            // Add section information to the data path
            dataPath = HANDLE_DATASET.prepImagePath(dataPath, section);

            // The url for the data request
            if (dataPath !== undefined && dataPath !== '' &&
                    dataPath !== false) {
                dataUrl = CONFIG_DATA.hdf5DataServer + dataPath;
            } else {
                dataUrl = CONFIG_DATA.hdf5DataServer;
            }

            if (debug) {
                console.log('  dataPath:  ' + dataPath);
                console.log('  dataUrl:   ' + dataUrl);
            }

            // Request the data from the server
            return $.when(SERVER_COMMUNICATION.ajaxRequest(dataUrl)).then(
                function (response) {

                    var bad_data = false;

                    if (debug) {
                        console.log("request finished, response:");
                        console.log(response);
                    }

                    // Set the data path in the url
                    window.history.pushState({}, document.title,
                        CONFIG_DATA.servicePath + '?data=' + dataPath);

                    // Check for a response indicating a problem with the
                    // reading of data
                    if (response.hasOwnProperty('error')) {
                        if (response.error !== "False") {
                            if (debug) {
                                console.log("  response.error:     " +
                                    response.error);
                                console.log("  response.item_type: " +
                                    response.item_type);
                            }

                            bad_data = true;

                            // Display an error message if there was something
                            // wrong with the reading of the data
                            DATA_DISPLAY.displayErrorMessage(response.error);
                        }
                    }

                    if (!bad_data) {

                        // How to display the result depends on the type of
                        // object
                        switch (response.item_type) {

                        case 'h5_file':
                        case 'folder':
                        case 'h5_folder':
                            HANDLE_DATASET.displayFolderContents(response,
                                fromPageLoad);
                            break;

                        case 'image-series':
                        case 'image':
                            HANDLE_DATASET.displayImage(response, dataUrl,
                                response.section, newImage,
                                response.short_name);
                            break;

                        case 'line':
                            HANDLE_DATASET.displayLine(response,
                                response.short_name);
                            break;

                        case 'number':
                            DATA_DISPLAY.drawText(response.short_name,
                                response.values, '#ad3a3a');
                            break;

                        case 'text':
                            DATA_DISPLAY.drawText(response.short_name,
                                response.values, '#3a74ad');
                            break;

                        default:
                            if (debug) {
                                console.log('Is this a dataset? No!');
                            }
                            DATA_DISPLAY.displayErrorMessage(dataUrl);
                        }
                    }

                    if (debug) {
                        console.debug('***  END  js/handle-dataset.js ' +
                            'requestForData');
                    }

                    return true;
                }
            );
        },


        // Given a dataset path, get all of it's ancestors' folder contents,
        // open the file tree to the dataset, and then finally get the
        // requested dataset and display it.
        getTreePath : function (dataPath, fromPageLoad) {

            var debug = false, cleanDataPath, highlightPath, dataUrl,
                errorMessage;

            if (debug) {
                console.debug('*** START handle-dataset.js getTreePath ***');
                console.log('  dataPath     : ' + dataPath);
                console.log('  fromPageLoad : ' + fromPageLoad);
            }

            AJAX_SPINNER.startLoadingData(1);

            // Remove any slice notation that may be present
            if (dataPath.includes('[')) {
                cleanDataPath = dataPath.substring(0, dataPath.indexOf('['));
            } else {
                cleanDataPath = dataPath;
            }

            // Remove trailing slashes as well, not needed
            if (cleanDataPath.charAt(cleanDataPath.length - 1) === '/') {
                cleanDataPath = cleanDataPath.substr(0,
                    cleanDataPath.length - 1);
            }

            if (debug) {
                console.log('  cleanDataPath: ' + cleanDataPath);
            }

            // Assemble the url
            dataUrl = CONFIG_DATA.hdf5DataServer + cleanDataPath +
                '?treepath=True';

            if (debug) {
                console.log('  dataUrl : ' + dataUrl);
            }

            // Get the full tree path information as well as some basic
            // information about the object, like what type it is and
            // whether it exists or not
            $.when(SERVER_COMMUNICATION.ajaxRequest(dataUrl)).then(
                function (response) {

                    var tree_path, tree_path_length = 0, i, key, value,
                        object_info;

                    if (debug) {
                        console.log('  response:');
                        console.log(response);
                    }

                    if (response.hasOwnProperty('tree_path')) {
                        tree_path = response.tree_path;
                        if (debug) {
                            console.log('tree_path:');
                            console.log(tree_path);
                        }
                        tree_path_length = Object.keys(tree_path).length;
                    }

                    if (debug) {
                        console.log('  tree_path_length: ' + tree_path_length);
                    }

                    if (response.hasOwnProperty('object_info')) {
                        object_info = response.object_info;
                        if (debug) {
                            console.log('object_info:');
                            console.log(object_info);
                        }
                    }


                    // Loop over each ancestor of this item
                    for (i = 0; i < tree_path_length; i += 1) {

                        // The keys are expected to be integers that have been
                        // converted to strings. Use them to get the values.
                        key = i.toString();
                        if (tree_path.hasOwnProperty(key)) {
                            value = tree_path[key];
                            if (debug) {
                                console.log(value);
                            }
                        }

                        // Add everything to the file tree, except for the
                        // first entry which is the root directory and should
                        // have already been displayed (hopefully - should add
                        // a check for this).
                        if (i > 0) {
                            FILE_NAV.addToTree(value.folder_contents,
                                value.path);
                        }
                    }

                    // Open the tree to the proper place, highlighting the
                    // selected item. This needs to wait a bit, otherwise there
                    // is some kind of collision with other graphical stuff
                    // going on - me thinks.
                    setTimeout(function () {

                        // Highlight to the last valid tree path element if the
                        // given data path is inaccessible
                        if (object_info.item_type === 'h5_unknown' ||
                                object_info.user_can_read === 'False' ||
                                object_info.does_exist === 'False') {
                            highlightPath = tree_path[key].path;

                        // Highlight the given data path
                        } else {
                            highlightPath = cleanDataPath;
                        }

                        if (debug) {
                            console.debug('  highlighting path: ' + '{' +
                                highlightPath + '}');
                        }

                        // Open the tree path and highlight the node item
                        highlightPath = decodeURIComponent(highlightPath)
                        if (highlightPath !== "") {
                            FILE_NAV.processSelectNodeEvent = false;
                            $('#jstree_div').jstree(true).select_node(
                                highlightPath
                            );
                        }

                        // Display an error messages if necessary
                        if (object_info.item_type === 'h5_unknown' ||
                                object_info.user_can_read === 'False' ||
                                object_info.does_exist === 'False') {

                            if (debug) {
                                console.log('Displaying an error message:');
                            }

                            if (object_info.item_type === 'h5_unknown' ||
                                    object_info.does_exist === 'False') {
                                errorMessage = 'Does Not Exist';
                            } else if (object_info.user_can_read === 'False') {
                                errorMessage = 'Permission Denied';
                            }

                            if (debug) {
                                console.log('  ' + errorMessage);
                            }

                            FILE_NAV.displayErrorMessage(cleanDataPath,
                                errorMessage);

                        // Now finally, get the requested data - maybe it's
                        // good to wait a bit for this too.
                        } else {

                            setTimeout(function () {
                                HANDLE_DATASET.requestForData(dataPath, false,
                                    true, fromPageLoad);
                            }, 100);

                        }

                        if (debug) {
                            console.debug('***  END  handle-dataset.js ' +
                                'getTreePath ***');
                        }

                    }, 500);

                }
            );

        },

        displayFolderContents : function (response, fromPageLoad) {

            var debug = false, folder_key, folder_item, folder_item_key,
                folder_item_info;

            if (debug) {
                console.debug('*** START handle-dataset.js ' +
                    'displayFolderContents() ***');
                console.log('  response:');
                console.log(response);
            }

            // Look for the 'folder_contents' section
            if (response.hasOwnProperty('folder_contents')) {

                // A little debugging...
                if (debug) {

                    // Loop over each item (folder, file, or dataset)
                    for (folder_key in response.folder_contents) {
                        if (response.folder_contents.hasOwnProperty(
                                folder_key
                            )) {

                            folder_item =
                                response.folder_contents[folder_key];

                            console.log('folder_key: ' + folder_key);

                            // Loop over each attribute of an item,
                            // name, type, etc.
                            for (folder_item_key in folder_item) {
                                if (folder_item.hasOwnProperty(
                                        folder_item_key
                                    )) {

                                    folder_item_info =
                                        folder_item[folder_item_key];

                                    console.log('  ' + folder_item_key
                                        + " -> " + folder_item_info);
                                }
                            }

                        }
                    }

                    console.log(response.folder_contents);
                }

                // Update the jstree object
                if (response.folder_contents !== false &&
                        !$.isEmptyObject(response.folder_contents)) {

                    FILE_NAV.addToTree(response.folder_contents,
                        response.path);

                    setTimeout(function () {
                        if (fromPageLoad) {
                            PAGE_LOAD.displayWelcomeMessage();
                        }
                    }, 100);

                } else {

                    if ($.isEmptyObject(response.folder_contents)) {

                        if (debug) {
                            console.log('folder contents are empty');
                        }

                        FILE_NAV.displayErrorMessage(response.path,
                            'Is empty!');
                    } else {
                        FILE_NAV.displayErrorMessage(response.path,
                            'Is not accessible!');
                    }
                }

            } else {
                if (debug) {
                    console.log('folder contents are empty');
                }

                FILE_NAV.displayErrorMessage(response.path,
                    'Is empty!');
            }

            AJAX_SPINNER.doneLoadingData(300);

            if (debug) {
                console.debug('***  END  handle-dataset.js ' +
                    'displayFolderContents() ***');
            }

        },

    };
