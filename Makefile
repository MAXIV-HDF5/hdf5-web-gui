###############################################################################
#
#  	Run 'make' for usage information
#
###############################################################################


# Makefile stuff
.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
THIS_FILE := $(lastword $(MAKEFILE_LIST))

export PYTHONPATH=.

# Pretty Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m


# Separator between output, 80 characters wide
define print_separator
    printf "$1"; printf "%0.s*" {1..80}; printf "$(NC)\n"
endef
print_line_green = $(call print_separator,$(GREEN))
print_line_blue = $(call print_separator,$(BLUE))
print_line_red = $(call print_separator,$(RED))

# Variables for making docker image tags
LAST_COMMIT_DATE = $(shell git log -n 1 --pretty=format:%cd --date=short -- \
    index.html \
    docker/Dockerfile \
    js/* \
    css/*)

LAST_COMMIT_HASH = $(shell git log -n 1 --pretty=format:%h -- \
    index.html \
    docker/Dockerfile \
    js/* \
    css/*)

TAG = $(LAST_COMMIT_DATE)_$(LAST_COMMIT_HASH)

REGISTRY=docker.maxiv.lu.se
PROJECT=maxiv-scisw-hdf5
NAME=hdf5-web-gui

GITLAB_REG=registry.gitlab.com
GITLAB_PROJ=maxiv-scisw/hdf5-viewer/hdf5-web-gui
GITLAB_NAME=hdf5-web-gui


##@ Help - usage and examples
.PHONY: help
help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)HDF5 Viewer $(CYAN)Makefile$(NC)\n\n"
	printf "    This is the front-end connection to the output from \n"
	printf "    hdf5-simple-server\n\n"
	printf "    This Makefile is used for running the applicaiton in two "
	printf "ways:\n"
	printf "		1) build and run a docker image\n"
	printf "		2) configure to run in development mode, without \n"
	printf "		   docker and with debugging output turned on\n"
	printf "    In both cases, configuration files will need to be modified\n"
	printf "    - see the README.\n"
	printf "\n$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	printf "\n$(BLUE)Examples$(NC)\n"
	printf "    $(CYAN)make $(NC)build$(NC)\n"
	printf "    $(CYAN)make $(NC)start-demo$(NC)\n"
	printf "    $(CYAN)make $(NC)start-cas-https$(NC)\n"
	printf "    $(CYAN)make $(NC)logs$(NC)\n"
	printf "    $(CYAN)make $(NC)stop$(NC)\n"
	# The awk command here is just to make the help message look nice
	@awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-21s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


##@ Build Image
.PHONY: build
build: ## Build the docker image
	@printf "\n"
	$(print_line_blue)
	docker build -t $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		-f docker/Dockerfile .
	$(print_line_blue)

.PHONY: tag-stable
tag-stable: ## Tag image for use in service
	docker image tag $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		$(PROJECT)/$(NAME):stable


##@ Start Container
.PHONY: start-demo
start-demo: ## Start docker container, use http, no authentication
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-demo.yml up -d
	$(print_line_green)

.PHONY: start-demo-maxiv
start-demo-maxiv: ## Use particular config file for MAX IV
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-demo-maxiv.yml up -d
	$(print_line_green)

.PHONY: start-cas-https
start-cas-https: ## Start docker container, use https, CAS authenticaion
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https.yml up -d
	$(print_line_green)

.PHONY: start-cas-https-maxiv
start-cas-https-maxiv: ## Use particular config file for MAX IV
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https-maxiv.yml up -d
	$(print_line_green)

.PHONY: start-cas-https-jasbru
start-cas-https-jasbru: ## Use particular config file for MAX IV
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https-jasbru.yml up -d
	$(print_line_green)


##@ Stop Container
.PHONY: stop
stop: ## Stop the docker container
	@printf "\n"
	$(print_line_red)
	docker-compose -f docker/docker-compose.yml  down
	$(print_line_red)

##@ Container Logs
.PHONY: logs
logs: ## Watch the docker container logs
	@printf "\n"
	$(print_line_blue)
	docker-compose -f docker/docker-compose.yml logs -ft
	$(print_line_blue)

##@ Container Connection
.PHONY: connect
connect: ## Connect to the running docker containter as root in a bash session
	@printf "\n"
	$(print_line_blue)
	docker exec -it maxiv-scisw-hdf5_hdf5-web-gui bash
	$(print_line_blue)


###############################################################################
##@ MAX IV CI
###############################################################################

.PHONY: push
push: ## Push image to registry
	@printf "\n"
	$(print_line_blue)
	docker push $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG)
	$(print_line_blue)


.PHONY: pull
pull: ## pull image from registry - current tag
	@printf "\n"
	$(print_line_blue)
	docker pull $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG)
	$(print_line_blue)


.PHONY: list-local
list-local: ## Tag image for push to registry
	@printf "\n"
	$(print_line_blue)
	docker image ls -f reference=$(PROJECT)/$(NAME)
	$(print_line_blue)
	docker image ls -f reference=$(REGISTRY)/$(PROJECT)/$(NAME)
	$(print_line_blue)
	docker image ls -f reference=$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME)
	$(print_line_blue)


.PHONY: list-registry
list-registry: ## Tag image for push to registry
	@printf "\n"
	$(print_line_blue)
	curl -sX GET https://$(REGISTRY)/v2/$(PROJECT)/$(NAME)/tags/list
	$(print_line_blue)


.PHONY: remove-local
remove-local: ## Remove all local docker images from this project
	@printf "\n"
	$(print_line_blue)
	docker image ls -f reference=$(PROJECT)/$(NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)
	docker image ls -f reference=$(REGISTRY)/$(PROJECT)/$(NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)
	docker image ls -f reference=$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)


###############################################################################
##@ GITLAB.COM
###############################################################################

.PHONY: tag-gitlab-com
tag-gitlab-com: ## Tag image for push public gitlab.com registry
	docker tag $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)


.PHONY: push-gitlab-com
push-gitlab-com: ## Push image to public gitlab.com registry
	@printf "\n"
	$(print_line_blue)
	docker push $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)
	$(print_line_blue)


.PHONY: pull-gitlab-com
pull-gitlab-com: ## pull image from gitlab.com registry - current tag
	@printf "\n"
	$(print_line_blue)
	docker pull $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)
	$(print_line_blue)


.PHONY: tag-gitlab-com-stable
tag-gitlab-com-stable: ## Tag image pulled from gitlab.com as stable
	docker tag $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG) \
		$(PROJECT)/$(NAME):stable


###############################################################################
##@ Development Setup & Usage
###############################################################################

.PHONY: configure-dev-demo configure-dev-cas-https
configure-dev-demo: ## Use configuration to run without docker and without https and authenticaion
	@printf "\n"
	$(print_line_blue)
	rm config/config.json
	ln -sf config-dev-demo.json config/config.json
	printf "Linked config: \n"
	ls -l config/config.json
	printf "\n"
	printf "cat config/config.json\n"
	cat config/config.json
	$(print_line_blue)

configure-dev-cas-https: ## Use configuration to run without docker, but with https and authenticaion
	@printf "\n"
	$(print_line_blue)
	rm config/config.json
	ln -sf config-dev-cas-https.json config/config.json
	printf "Linked config: \n"
	ls -l config/config.json
	printf "\n"
	printf "cat config/config.json\n"
	cat config/config.json
	$(print_line_blue)


###############################################################################
##@ Errors
###############################################################################

cgroup-error-fix: ## Fix cgroup mountpoint docker error! Debian only issue?
	@$(print_line_green)

	systemd_dir=/sys/fs/cgroup/systemd
	if [ ! -d "$$systemd_dir" ]; then
		printf "$(GREEN)**$(CYAN) Fixing Error:$(NC)\n"
		printf "$(GREEN)**   $(RED) cgroup mountpoint does not exist: unknown$(NC)\n"
		printf "    \n"

		printf "$(RED)  $$systemd_dir does not exist\n$(NC)"
		printf "  Will create it now:\n"
		sudo mkdir $$systemd_dir
		sudo mount -t cgroup -o none,name=systemd cgroup $$systemd_dir

		printf "$(GREEN)**$(CYAN) Done Fixing Error$(NC)\n"
	else
		printf "$(GREEN)  $$systemd_dir exists    \n$(NC)"
		printf "  Maybe the issue is something else?\n"
	fi

	$(print_line_green)
